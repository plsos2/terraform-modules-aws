terraform {
  required_providers {
    aws      = "~> 2.70.0"
    local    = "~> 1.0"
    random   = "~> 3.0"
    template = "~> 2.0"
    tls      = "~> 2.0"
  }
}

locals {
  environment  = "test"
  project_name = "test-vpc-finder"
  region       = "us-east-2"
}

provider "aws" {
  region  = local.region
  profile = "proplogix"
}

module "vpc" {
  source      = "../aws/vpc-finder"
  environment = local.environment
  vpc_name = "proplogix-poc-vpc"
}
