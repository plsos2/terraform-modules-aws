## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |

## Providers

| Name | Version |
|------|---------|
| tls | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| algorithm | Algorithm | `string` | `"RSA"` | no |
| ecdsa\_curve | EC-DSA | `string` | `"P224"` | no |
| rsa\_bits | RSA bits | `string` | `4096` | no |

## Outputs

| Name | Description |
|------|-------------|
| fingerprint | n/a |
| private\_key\_pem | n/a |
| public\_key\_openssh | n/a |
| public\_key\_pem | n/a |

