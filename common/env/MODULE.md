## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |

## Providers

No provider.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| dns\_host\_name | DNS Host Name | `string` | `null` | no |
| domain | Domain name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| domain | n/a |
| domain\_name | n/a |
| domain\_root | n/a |
| domain\_slug | n/a |
| env\_domain\_name | n/a |
| env\_domain\_slug | n/a |
| env\_name | n/a |
| is\_dev | n/a |
| is\_production | n/a |
| is\_stage | n/a |
| not\_dev | n/a |
| not\_production | n/a |
| not\_stage | n/a |

