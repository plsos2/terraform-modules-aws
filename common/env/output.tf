output "domain" {
  value = local.domain_name
}

output "domain_name" {
  value = local.domain_name
}

output "domain_root" {
  value = local.domain_root
}

output "domain_slug" {
  value = local.domain_slug
}

output "env_domain_name" {
  value = local.env_domain_name
}

output "env_domain_slug" {
  value = local.env_domain_slug
}

output "env_name" {
  value = local.env_name
}

output "is_dev" {
  value = local.is_dev
}

output "is_stage" {
  value = local.is_stage
}

output "is_production" {
  value = local.is_production
}

output "not_dev" {
  value = local.not_dev
}

output "not_stage" {
  value = local.not_stage
}

output "not_production" {
  value = local.not_production
}
