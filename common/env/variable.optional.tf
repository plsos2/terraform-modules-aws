variable "dns_host_name" {
  default     = null
  description = "DNS Host Name"
  type        = string
}
