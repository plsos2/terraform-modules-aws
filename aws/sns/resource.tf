resource "aws_sns_topic" "sns_topic" {
  count = length(var.sns_topics)
  name  = var.sns_topics[count.index]
}