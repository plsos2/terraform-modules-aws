variable "sns_topics" {
  description = "A list of SNS Topics that will be created and subscribed to by the Lambda Function."
  type        = list(string)
}