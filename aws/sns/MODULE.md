## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| sns\_topics | A list of SNS Topics that will be created and subscribed to by the Lambda Function. | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| sns\_topics | n/a |

