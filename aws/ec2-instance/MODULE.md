## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |
| random | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allowed\_hosts | Allowed Hosts | `list(string)` | `[]` | no |
| ami | AMI | `string` | n/a | yes |
| associate\_public\_ip\_address | Associate Public IP Address | `bool` | `true` | no |
| availability\_zone | Availability Zone | `string` | `null` | no |
| computer\_name | Computer Name | `string` | `null` | no |
| connection\_https | Use HTTPS Connection | `bool` | `true` | no |
| connection\_insecure | Use Insecure Connection | `bool` | `true` | no |
| connection\_timeout | Connection Timeout | `string` | `"5m"` | no |
| cpu\_core\_count | CPU Core Count | `number` | `null` | no |
| cpu\_threads\_per\_core | CPU Threads Per Core | `number` | `null` | no |
| disable\_api\_termination | Disable API Termination | `bool` | `false` | no |
| domain | Domain Name | `string` | n/a | yes |
| ebs\_block\_devices | EBS Block Devices | <pre>list(object({<br>    delete_on_termination = bool<br>    encrypted             = bool<br>    iops                  = number<br>    kms_key_id            = string<br>    name                  = string<br>    snapshot_id           = string<br>    volume_id             = string<br>    volume_size           = number<br>  }))</pre> | `[]` | no |
| get\_password\_data | Get Password Data | `bool` | `true` | no |
| host\_id | Host ID | `string` | `null` | no |
| iam\_instance\_profile | IAM Instance Profile | `string` | `null` | no |
| ingress | Ingress Ports | <pre>list(object({<br>    cidr_blocks     = list(string)<br>    description     = string<br>    from_port       = number<br>    protocol        = string<br>    to_port         = number<br>    security_groups = list(string)<br>  }))</pre> | `[]` | no |
| instance\_initiated\_shutdown\_behavior | Instance Initiated Shutdown Behavior | `string` | `"stop"` | no |
| instance\_type | Instance Type | `string` | `"t2.micro"` | no |
| key\_name | Key Name | `string` | `null` | no |
| key\_pem | Key PEM | `string` | `null` | no |
| monitoring | Monitoring | `bool` | `false` | no |
| password | Password | `string` | n/a | yes |
| placement\_group | Placement Group | `string` | `null` | no |
| private\_ip | Private IP | `string` | `null` | no |
| project\_name | Project Name | `string` | n/a | yes |
| provision\_port\_rdp | RDP Port | `number` | `3389` | no |
| provision\_port\_winrm | WinRM Port | `number` | `5985` | no |
| provision\_port\_winrm\_ssl | WinRM SSL Port | `number` | `5986` | no |
| provisioner\_file\_destination | Provisioner File Destination | `string` | n/a | yes |
| provisioner\_file\_source | Provisioner File Source | `string` | n/a | yes |
| provisioner\_remote\_exec | Provisioner Remote Exec | `list(string)` | n/a | yes |
| root\_delete\_on\_termination | Delete on Termination | `bool` | `true` | no |
| root\_encrypted | Encrypted | `bool` | `false` | no |
| root\_iops | IOPS | `number` | `0` | no |
| root\_kms\_key\_id | KMS Key ID | `string` | `null` | no |
| root\_volume\_id | Volume ID | `string` | `null` | no |
| root\_volume\_size | Volume Size | `number` | `128` | no |
| root\_volume\_type | Volume Type | `string` | `"gp2"` | no |
| security\_groups | Security Groups | `list(string)` | `[]` | no |
| subnet\_id | Subnet ID | `string` | `null` | no |
| tenancy | Tenancy | `string` | `"default"` | no |
| user\_data | User Data | `string` | n/a | yes |
| user\_data\_base64 | User Data Base64 | `string` | `null` | no |
| vpc\_id | VPC ID | `string` | n/a | yes |
| vpc\_security\_group\_ids | VPC Security Group IDs | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| arn | n/a |
| availability\_zone | n/a |
| id | n/a |
| instance\_state | n/a |
| ipv6\_addresses | n/a |
| key\_name | n/a |
| password | n/a |
| password\_data | n/a |
| placement\_group | n/a |
| primary\_network\_interface\_id | n/a |
| private\_dns | n/a |
| private\_ip | n/a |
| public\_dns | n/a |
| public\_ip | n/a |
| security\_groups | n/a |
| subnet\_id | n/a |
| vpc\_security\_group\_ids | n/a |

