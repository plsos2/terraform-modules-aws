variable "root_delete_on_termination" {
  default     = true
  description = "Delete on Termination"
  type        = bool
}

variable "root_encrypted" {
  default     = false
  description = "Encrypted"
  type        = bool
}

variable "root_iops" {
  default     = 0
  description = "IOPS"
  type        = number
}

variable "root_kms_key_id" {
  default     = null
  description = "KMS Key ID"
  type        = string
}

variable "root_volume_id" {
  default     = null
  description = "Volume ID"
  type        = string
}

variable "root_volume_size" {
  default     = 128
  description = "Volume Size"
  type        = number
}

variable "root_volume_type" {
  default     = "gp2"
  description = "Volume Type"
  type        = string
}
