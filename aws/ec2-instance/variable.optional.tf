variable "allowed_hosts" {
  default     = []
  description = "Allowed Hosts"
  type        = list(string)
}

variable "associate_public_ip_address" {
  default     = true
  description = "Associate Public IP Address"
  type        = bool
}

variable "availability_zone" {
  default     = null
  description = "Availability Zone"
  type        = string
}

variable "computer_name" {
  default     = null
  description = "Computer Name"
  type        = string
}

variable "connection_https" {
  default     = true
  description = "Use HTTPS Connection"
  type        = bool
}

variable "connection_insecure" {
  default     = true
  description = "Use Insecure Connection"
  type        = bool
}

variable "connection_timeout" {
  default     = "5m"
  description = "Connection Timeout"
  type        = string
}

variable "cpu_core_count" {
  default     = null
  description = "CPU Core Count"
  type        = number
}

variable "cpu_threads_per_core" {
  default     = null
  description = "CPU Threads Per Core"
  type        = number
}

variable "disable_api_termination" {
  default     = false
  description = "Disable API Termination"
  type        = bool
}

variable "get_password_data" {
  default     = true
  description = "Get Password Data"
  type        = bool
}

variable "host_id" {
  default     = null
  description = "Host ID"
  type        = string
}

variable "iam_instance_profile" {
  default     = null
  description = "IAM Instance Profile"
  type        = string
}

variable "ingress" {
  default     = []
  description = "Ingress Ports"

  type = list(object({
    cidr_blocks     = list(string)
    description     = string
    from_port       = number
    protocol        = string
    to_port         = number
    security_groups = list(string)
  }))
}

variable "instance_initiated_shutdown_behavior" {
  default     = "stop"
  description = "Instance Initiated Shutdown Behavior"
  type        = string
}

variable "instance_type" {
  default     = "t2.micro"
  description = "Instance Type"
  type        = string
}

variable "key_name" {
  default     = null
  description = "Key Name"
  type        = string
}

variable "key_pem" {
  default     = null
  description = "Key PEM"
  type        = string
}

variable "monitoring" {
  default     = false
  description = "Monitoring"
  type        = bool
}

variable "placement_group" {
  default     = null
  description = "Placement Group"
  type        = string
}

variable "private_ip" {
  default     = null
  description = "Private IP"
  type        = string
}

variable "provision_port_rdp" {
  default     = 3389
  description = "RDP Port"
  type        = number
}

variable "provision_port_winrm" {
  default     = 5985
  description = "WinRM Port"
  type        = number
}

variable "provision_port_winrm_ssl" {
  default     = 5986
  description = "WinRM SSL Port"
  type        = number
}

variable "security_groups" {
  default     = []
  description = "Security Groups"
  type        = list(string)
}

variable "subnet_id" {
  default     = null
  description = "Subnet ID"
  type        = string
}

variable "tenancy" {
  default     = "default"
  description = "Tenancy"
  type        = string
}

variable "user_data_base64" {
  default     = null
  description = "User Data Base64"
  type        = string
}

variable "vpc_security_group_ids" {
  default     = []
  description = "VPC Security Group IDs"
  type        = list(string)
}
