resource "aws_security_group" "instance" {
  description = "Allow Instance Access"
  name_prefix = "ec2-${var.domain}-"
  vpc_id      = var.vpc_id

  tags = {
    Name    = var.domain
    Project = var.project_name
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
  }

  ingress {
    cidr_blocks = length(var.allowed_hosts) > 0 ? var.allowed_hosts : ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = var.provision_port_rdp
    to_port     = var.provision_port_rdp
  }

  ingress {
    cidr_blocks = length(var.allowed_hosts) > 0 ? var.allowed_hosts : ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = var.provision_port_winrm
    to_port     = var.provision_port_winrm
  }

  ingress {
    cidr_blocks = length(var.allowed_hosts) > 0 ? var.allowed_hosts : ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = var.provision_port_winrm_ssl
    to_port     = var.provision_port_winrm_ssl
  }

  dynamic "ingress" {
    for_each = range(0, length(var.ingress))

    content {
      cidr_blocks     = var.ingress[ingress.value].cidr_blocks
      description     = var.ingress[ingress.value].description
      protocol        = var.ingress[ingress.value].protocol
      from_port       = var.ingress[ingress.value].from_port
      to_port         = var.ingress[ingress.value].to_port
      security_groups = var.ingress[ingress.value].security_groups
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "random_id" "device_name" {
  byte_length = 12
}

resource "aws_instance" "instance" {
  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  availability_zone           = var.availability_zone
  cpu_core_count              = var.cpu_core_count
  cpu_threads_per_core        = var.cpu_threads_per_core
  disable_api_termination     = var.disable_api_termination
  get_password_data           = var.get_password_data
  host_id                     = var.host_id
  iam_instance_profile        = var.iam_instance_profile
  instance_type               = var.instance_type
  key_name                    = var.key_name
  monitoring                  = var.monitoring
  placement_group             = var.placement_group
  private_ip                  = var.private_ip
  subnet_id                   = var.subnet_id
  tenancy                     = var.tenancy
  user_data                   = var.user_data
  user_data_base64            = var.user_data_base64
  vpc_security_group_ids      = concat([aws_security_group.instance.id], var.security_groups)

  tags = {
    Name    = coalesce(var.computer_name, var.domain)
    Project = var.project_name
  }

  connection {
    type     = "winrm"
    host     = aws_instance.instance.public_ip
    https    = var.connection_https
    insecure = var.connection_insecure
    password = var.password
    timeout  = var.connection_timeout
    user     = "terraform"
  }

  dynamic "ebs_block_device" {
    iterator = ebs
    for_each = range(0, length(var.ebs_block_devices))

    content {
      delete_on_termination = var.ebs_block_devices[ebs.value].delete_on_termination
      device_name           = coalesce(var.ebs_block_devices[ebs.value].ebs_name, random_id.device_name.hex)
      encrypted             = var.ebs_block_devices[ebs.value].ebs_encrypted
      iops                  = var.ebs_block_devices[ebs.value].ebs_iops
      kms_key_id            = var.ebs_block_devices[ebs.value].ebs_kms_key_id
      snapshot_id           = var.ebs_block_devices[ebs.value].ebs_snapshot_id
      volume_id             = var.ebs_block_devices[ebs.value].ebs_volume_id
      volume_size           = var.ebs_block_devices[ebs.value].ebs_volume_size
      volume_type           = var.ebs_block_devices[ebs.value].ebs_volume_type
    }
  }

  root_block_device {
    delete_on_termination = var.root_delete_on_termination
    encrypted             = var.root_encrypted
    iops                  = var.root_iops
    kms_key_id            = var.root_kms_key_id
    volume_id             = var.root_volume_id
    volume_size           = var.root_volume_size
    volume_type           = var.root_volume_type
  }

  provisioner "file" {
    destination = var.provisioner_file_destination
    source      = var.provisioner_file_source
  }

  provisioner "remote-exec" {
    inline = var.provisioner_remote_exec
  }
}
