variable "ebs_block_devices" {
  default     = []
  description = "EBS Block Devices"
  type = list(object({
    delete_on_termination = bool
    encrypted             = bool
    iops                  = number
    kms_key_id            = string
    name                  = string
    snapshot_id           = string
    volume_id             = string
    volume_size           = number
  }))
}
