variable "ami" {
  description = "AMI"
  type        = string
}

variable "domain" {
  description = "Domain Name"
  type        = string
}

variable "password" {
  description = "Password"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "provisioner_file_destination" {
  description = "Provisioner File Destination"
  type        = string
}

variable "provisioner_file_source" {
  description = "Provisioner File Source"
  type        = string
}

variable "provisioner_remote_exec" {
  description = "Provisioner Remote Exec"
  type        = list(string)
}

variable "user_data" {
  description = "User Data"
  type        = string
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}
