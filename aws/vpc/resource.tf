resource "aws_vpc" "default" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
  instance_tenancy     = var.instance_tenancy

  tags = {
    Name        = "vpc-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_eip" "default" {
  vpc = true

  tags = {
    Name        = "eip-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id

  tags = {
    Name        = "ig-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_nat_gateway" "default" {
  depends_on    = [aws_vpc.default]
  allocation_id = aws_eip.default.id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name        = "nat-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.default.id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.default.id

  timeouts {
    create = "5m"
  }
}

resource "aws_subnet" "private" {
  count                   = length(var.availability_zones)
  availability_zone       = element(var.availability_zones, count.index)
  cidr_block              = element(var.vpc_cidr_private_block_subnets, count.index)
  map_public_ip_on_launch = false
  vpc_id                  = aws_vpc.default.id

  tags = {
    Name        = "private-${element(var.availability_zones, count.index)}-${var.environment}"
    Project     = var.project_name
    Subnet      = "private"
    Environment = var.environment
  }
}

resource "aws_subnet" "public" {
  count                   = length(var.availability_zones)
  availability_zone       = element(var.availability_zones, count.index)
  cidr_block              = element(var.vpc_cidr_public_block_subnets, count.index)
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.default.id

  tags = {
    Name        = "public-${element(var.availability_zones, count.index)}-${var.environment}"
    Project     = var.project_name
    Subnet      = "public"
    Environment = var.environment
  }
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name       = local.db_subnet_group_name
  subnet_ids = concat(aws_subnet.private[*].id, aws_subnet.public[*].id)

  tags = {
    Name        = var.environment
    Project     = var.project_name
    Environment = var.environment
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.default.id

  tags = {
    Name        = "private-${var.environment}"
    Project     = var.project_name
    Environment = var.environment
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.default.id

  tags = {
    Name        = "public-${var.environment}"
    Project     = var.project_name
    Environment = var.environment
  }
}

resource "aws_route_table_association" "private_route_associations" {
  count          = length(aws_subnet.private)
  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "public_route_associations" {
  count          = length(aws_subnet.public)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}
