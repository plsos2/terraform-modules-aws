variable "availability_zones" {
  description = "Availablility Zones"
  type        = list(string)
}

variable "environment" {
  description = "Environment Name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "vpc_cidr_block" {
  description = "VPC CIDR Block"
  type        = string
}

variable "vpc_cidr_private_block_subnets" {
  description = "VPC CIDR Block Subnets"
  type        = list(string)
}

variable "vpc_cidr_public_block_subnets" {
  description = "VPC CIDR Block Subnets"
  type        = list(string)
}
