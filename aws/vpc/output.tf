output "arn" {
  value = aws_vpc.default.arn
}

output "availability_zones" {
  value = var.availability_zones
}

output "cidr_block" {
  value = aws_vpc.default.cidr_block
}

output "db_subnet_group_name" {
  value = local.db_subnet_group_name
}

output "db_subnet_group_arn" {
  value = aws_db_subnet_group.db_subnet_group.arn
}

output "db_subnet_group_id" {
  value = aws_db_subnet_group.db_subnet_group.id
}

output "default_network_acl_id" {
  value = aws_vpc.default.default_network_acl_id
}

output "default_route_table_id" {
  value = aws_vpc.default.default_route_table_id
}

output "default_security_group_id" {
  value = aws_vpc.default.default_security_group_id
}

output "gateway_id" {
  value = aws_internet_gateway.default.id
}

output "main_route_table_id" {
  value = aws_vpc.default.main_route_table_id
}

output "nat_gateway_id" {
  value = aws_nat_gateway.default.id
}

output "private" {
  value = aws_subnet.private
}

output "public" {
  value = aws_subnet.public
}

output "subnets" {
  value = concat(aws_subnet.public[*].id, aws_subnet.private[*].id)
}

output "vpc_id" {
  value = aws_vpc.default.id
}
