## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| availability\_zones | Availablility Zones | `list(string)` | n/a | yes |
| enable\_dns\_hostnames | Enable DNS Hostnames | `bool` | `true` | no |
| enable\_dns\_support | Enable DNS Support | `bool` | `true` | no |
| environment | Environment Name | `string` | n/a | yes |
| instance\_tenancy | Instance Tenancy | `string` | `"default"` | no |
| project\_name | Project Name | `string` | n/a | yes |
| vpc\_cidr\_block | VPC CIDR Block | `string` | n/a | yes |
| vpc\_cidr\_private\_block\_subnets | VPC CIDR Block Subnets | `list(string)` | n/a | yes |
| vpc\_cidr\_public\_block\_subnets | VPC CIDR Block Subnets | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn | n/a |
| availability\_zones | n/a |
| cidr\_block | n/a |
| db\_subnet\_group\_arn | n/a |
| db\_subnet\_group\_id | n/a |
| db\_subnet\_group\_name | n/a |
| default\_network\_acl\_id | n/a |
| default\_route\_table\_id | n/a |
| default\_security\_group\_id | n/a |
| gateway\_id | n/a |
| main\_route\_table\_id | n/a |
| nat\_gateway\_id | n/a |
| private | n/a |
| public | n/a |
| subnets | n/a |
| vpc\_id | n/a |

