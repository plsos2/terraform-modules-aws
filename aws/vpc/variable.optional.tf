variable "enable_dns_hostnames" {
  default     = true
  description = "Enable DNS Hostnames"
  type        = bool
}

variable "enable_dns_support" {
  default     = true
  description = "Enable DNS Support"
  type        = bool
}

variable "instance_tenancy" {
  default     = "default"
  description = "Instance Tenancy"
  type        = string
}
