variable "allow_major_version_upgrade" {
  default     = false
  description = "Allow Major Version Upgrade"
  type        = bool
}

variable "allowed_hosts" {
  default     = []
  description = "Allowed Hosts"
  type        = list(string)
}

variable "apply_immediately" {
  default     = true
  description = "Apply Immediately"
  type        = bool
}

variable "auto_minor_version_upgrade" {
  default     = false
  description = "Allow automatic upgrades"
  type        = bool
}

variable "availability_zone" {
  default     = null
  description = "Availability Zone"
  type        = string
}

variable "backup_retention_period" {
  default     = 0
  description = "Backup Retention Period"
  type        = number
}

variable "backup_window" {
  default     = null
  description = "Backup Window"
  type        = string
}

variable "character_set_name" {
  default     = null
  description = "Character Set Name"
  type        = string
}

variable "copy_tags_to_snapshot" {
  default     = false
  description = "Copy Tags to Snapshot"
  type        = bool
}

variable "database_port" {
  default     = 3306
  description = "Database Port"
  type        = number
}

variable "db_subnet_group_name" {
  default     = null
  description = "Database Subnet Group Name"
  type        = string
}

variable "deletion_protection" {
  default     = false
  description = "Deletion Protection"
  type        = bool
}

variable "disk_size" {
  default     = 128
  description = "Disk size"
  type        = string
}

variable "enabled_cloudwatch_logs_exports" {
  default     = "error"
  description = "Enabled CloudWatch Logs Exports"
  type        = string
}

variable "enable_monitoring" {
  default     = false
  description = "Enable Monitoring"
  type        = bool
}

variable "event_categories" {
  default     = ["availability", "deletion", "failure", "maintenance"]
  description = "Event Categories"
  type        = list(string)
}

variable "iam_database_authentication_enabled" {
  default     = false
  description = "IAM Database Authentication Enabled"
  type        = bool
}

variable "instance_class" {
  default     = "db.t2.small"
  description = "Instance Class"
  type        = string
}

variable "iops" {
  default     = 0
  description = "IOPS"
  type        = number
}

variable "kms_key_id" {
  default     = null
  description = "KMS Key ID"
  type        = string
}

variable "license_model" {
  default     = "general-public-license"
  description = "License Model"
  type        = string
}

variable "maintenance_window" {
  default     = null
  description = "Maintenance Window"
  type        = string
}

variable "monitoring_interval" {
  default     = 0
  description = "Monitoring Interval"
  type        = number
}

variable "monitoring_role_arn" {
  default     = null
  description = "Monitoring Role ARN"
  type        = string
}

variable "monitoring_role_name" {
  default     = "rds-monitoring-role"
  description = "Monitoring Role Name"
  type        = string
}

variable "multi_az" {
  default     = false
  description = "Multi-AZ"
  type        = bool
}

variable "options" {
  default     = []
  description = "DB Options"

  type = list(
    object(
      {
        db_security_group_memberships  = list(string)
        option_name                    = string
        port                           = number
        version                        = string
        vpc_security_group_memberships = list(string)

        option_settings = list(
          object(
            {
              name  = string
              value = string
            }
          )
        )
      }
    )
  )
}

variable "parameters" {
  default     = []
  description = "DB Parameters"

  type = list(
    object(
      {
        apply_method = string
        name         = string
        value        = string
      }
    )
  )
}

variable "performance_insights_enabled" {
  default     = false
  description = "Performance Insights Enabled"
  type        = bool
}

variable "performance_insights_kms_key_id" {
  default     = null
  description = "Performance Insights KMS Key ID"
  type        = string
}

variable "performance_insights_retention_period" {
  default     = 0
  description = "Performance Insights Retention Period"
  type        = number
}

variable "publicly_accessible" {
  default     = false
  description = "Publicly Accessible"
  type        = bool
}

variable "retention_in_days" {
  default     = 90
  description = "Retention in Days"
  type        = number
}

variable "retention_time" {
  default     = 0
  description = "Retention Time"
  type        = number
}

variable "security_groups" {
  default     = []
  description = "Security Groups"
  type        = list(string)
}

variable "skip_final_snapshot" {
  default     = true
  description = "Skip Final Snapshot"
  type        = bool
}

variable "snapshot_identifier" {
  default     = null
  description = "Snapshot Identifier"
  type        = string
}

variable "storage_encrypted" {
  default     = false
  description = "Storage Encryption Enabled"
  type        = bool
}

variable "storage_type" {
  default     = "gp2"
  description = "Storage Type"
  type        = string
}

variable "username" {
  default     = "admin"
  description = "Database username"
  type        = string
}

variable "vpc_security_group_ids" {
  default     = []
  description = "VPC Security Groups IDs"
  type        = list(string)
}
