variable "database_identifier" {
  description = "Database Identifier"
  type        = string
}

variable "database_name" {
  description = "Database Name"
  type        = string
}

variable "domain" {
  description = "Domain name"
  type        = string
}

variable "bucket_arn" {
  description = "Bucket ARN"
  type        = string
}

variable "engine" {
  description = "Engine"
  type        = string
}

variable "engine_version" {
  description = "Engine Version"
  type        = string
}

variable "major_engine_version" {
  description = "Major Engine Version"
  type        = string
}

variable "password" {
  description = "Database password"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}
