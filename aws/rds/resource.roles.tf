data "aws_iam_policy_document" "rds" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "rds" {
  assume_role_policy = data.aws_iam_policy_document.rds.json
  name               = "rds-${var.database_identifier}"

  tags = {
    Name    = var.domain
    Project = var.project_name
  }
}

data "aws_iam_policy_document" "s3" {
  statement {
    actions = [
      "s3:ListAllMyBuckets",
    ]
    effect    = "Allow"
    resources = ["*"]
  }

  statement {
    actions = [
      "s3:GetBucketACL",
      "s3:GetBucketLocation",
      "s3:ListBuckets",
    ]
    effect    = "Allow"
    resources = [var.bucket_arn]
  }

  statement {
    actions = [
      "s3:AbortMultipartUpload",
      "s3:ListMultipartUploadParts",
      "s3:PutObject",
    ]
    effect    = "Allow"
    resources = ["${var.bucket_arn}/*"]
  }
}

resource "aws_iam_policy" "s3" {
  name   = "rds-s3-${var.database_identifier}"
  policy = data.aws_iam_policy_document.s3.json
}

resource "aws_iam_role_policy_attachment" "s3" {
  role       = aws_iam_role.rds.name
  policy_arn = aws_iam_policy.s3.arn
}

data "aws_iam_policy_document" "enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "enhanced_monitoring" {
  count              = var.enable_monitoring ? 1 : 0
  assume_role_policy = data.aws_iam_policy_document.enhanced_monitoring.json
  name               = "rds-${var.monitoring_role_name}-${var.database_identifier}"

  tags = {
    Name     = var.domain
    Project  = var.project_name
    RoleName = format("%s", var.monitoring_role_name)
  }
}

resource "aws_iam_role_policy_attachment" "enhanced_monitoring" {
  count      = var.enable_monitoring ? 1 : 0
  role       = aws_iam_role.enhanced_monitoring[count.index].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}
