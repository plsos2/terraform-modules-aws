output "address" {
  value = aws_db_instance.database.address
}

output "arn" {
  value = aws_db_instance.database.arn
}

output "allocated_storage" {
  value = aws_db_instance.database.allocated_storage
}

output "availability_zone" {
  value = aws_db_instance.database.availability_zone
}

output "backup_retention_period" {
  value = aws_db_instance.database.backup_retention_period
}

output "backup_window" {
  value = aws_db_instance.database.backup_window
}

output "ca_cert_identifier" {
  value = aws_db_instance.database.ca_cert_identifier
}

output "domain" {
  value = aws_db_instance.database.domain
}

output "domain_iam_role_name" {
  value = aws_db_instance.database.domain_iam_role_name
}

output "endpoint" {
  value = replace(
    aws_db_instance.database.endpoint,
    ":${aws_db_instance.database.port}",
    "",
  )
}

output "engine" {
  value = aws_db_instance.database.engine
}

output "engine_version" {
  value = aws_db_instance.database.engine_version
}

output "hosted_zone_id" {
  value = aws_db_instance.database.hosted_zone_id
}

output "id" {
  value = aws_db_instance.database.id
}

output "instance_class" {
  value = aws_db_instance.database.instance_class
}

output "maintenance_window" {
  value = aws_db_instance.database.maintenance_window
}

output "multi_az" {
  value = aws_db_instance.database.multi_az
}

output "name" {
  value = aws_db_instance.database.name
}

output "port" {
  value = aws_db_instance.database.port
}

output "resource_id" {
  value = aws_db_instance.database.resource_id
}

output "security_groups" {
  value = [aws_security_group.database.name]
}

output "security_group_ids" {
  value = [aws_security_group.database.id]
}

output "sns_arn" {
  value = aws_sns_topic.rds.arn
}

output "sns_id" {
  value = aws_sns_topic.rds.id
}

output "status" {
  value = aws_db_instance.database.status
}

output "storage_encrypted" {
  value = aws_db_instance.database.storage_encrypted
}

output "username" {
  value = aws_db_instance.database.username
}
