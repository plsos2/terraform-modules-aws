## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |
| random | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allow\_major\_version\_upgrade | Allow Major Version Upgrade | `bool` | `false` | no |
| allowed\_hosts | Allowed Hosts | `list(string)` | `[]` | no |
| apply\_immediately | Apply Immediately | `bool` | `true` | no |
| auto\_minor\_version\_upgrade | Allow automatic upgrades | `bool` | `false` | no |
| availability\_zone | Availability Zone | `string` | `null` | no |
| backup\_retention\_period | Backup Retention Period | `number` | `0` | no |
| backup\_window | Backup Window | `string` | `null` | no |
| bucket\_arn | Bucket ARN | `string` | n/a | yes |
| character\_set\_name | Character Set Name | `string` | `null` | no |
| copy\_tags\_to\_snapshot | Copy Tags to Snapshot | `bool` | `false` | no |
| database\_identifier | Database Identifier | `string` | n/a | yes |
| database\_name | Database Name | `string` | n/a | yes |
| database\_port | Database Port | `number` | `3306` | no |
| db\_subnet\_group\_name | Database Subnet Group Name | `string` | `null` | no |
| deletion\_protection | Deletion Protection | `bool` | `false` | no |
| disk\_size | Disk size | `string` | `128` | no |
| domain | Domain name | `string` | n/a | yes |
| enable\_monitoring | Enable Monitoring | `bool` | `false` | no |
| enabled\_cloudwatch\_logs\_exports | Enabled CloudWatch Logs Exports | `string` | `"error"` | no |
| engine | Engine | `string` | n/a | yes |
| engine\_version | Engine Version | `string` | n/a | yes |
| event\_categories | Event Categories | `list(string)` | <pre>[<br>  "availability",<br>  "deletion",<br>  "failure",<br>  "maintenance"<br>]</pre> | no |
| iam\_database\_authentication\_enabled | IAM Database Authentication Enabled | `bool` | `false` | no |
| instance\_class | Instance Class | `string` | `"db.t2.small"` | no |
| iops | IOPS | `number` | `0` | no |
| kms\_key\_id | KMS Key ID | `string` | `null` | no |
| license\_model | License Model | `string` | `"general-public-license"` | no |
| maintenance\_window | Maintenance Window | `string` | `null` | no |
| major\_engine\_version | Major Engine Version | `string` | n/a | yes |
| monitoring\_interval | Monitoring Interval | `number` | `0` | no |
| monitoring\_role\_arn | Monitoring Role ARN | `string` | `null` | no |
| monitoring\_role\_name | Monitoring Role Name | `string` | `"rds-monitoring-role"` | no |
| multi\_az | Multi-AZ | `bool` | `false` | no |
| options | DB Options | <pre>list(<br>    object(<br>      {<br>        db_security_group_memberships  = list(string)<br>        option_name                    = string<br>        port                           = number<br>        version                        = string<br>        vpc_security_group_memberships = list(string)<br><br>        option_settings = list(<br>          object(<br>            {<br>              name  = string<br>              value = string<br>            }<br>          )<br>        )<br>      }<br>    )<br>  )</pre> | `[]` | no |
| parameters | DB Parameters | <pre>list(<br>    object(<br>      {<br>        apply_method = string<br>        name         = string<br>        value        = string<br>      }<br>    )<br>  )</pre> | `[]` | no |
| password | Database password | `string` | n/a | yes |
| performance\_insights\_enabled | Performance Insights Enabled | `bool` | `false` | no |
| performance\_insights\_kms\_key\_id | Performance Insights KMS Key ID | `string` | `null` | no |
| performance\_insights\_retention\_period | Performance Insights Retention Period | `number` | `0` | no |
| project\_name | Project Name | `string` | n/a | yes |
| publicly\_accessible | Publicly Accessible | `bool` | `false` | no |
| retention\_in\_days | Retention in Days | `number` | `90` | no |
| retention\_time | Retention Time | `number` | `0` | no |
| security\_groups | Security Groups | `list(string)` | `[]` | no |
| skip\_final\_snapshot | Skip Final Snapshot | `bool` | `true` | no |
| snapshot\_identifier | Snapshot Identifier | `string` | `null` | no |
| storage\_encrypted | Storage Encryption Enabled | `bool` | `false` | no |
| storage\_type | Storage Type | `string` | `"gp2"` | no |
| username | Database username | `string` | `"admin"` | no |
| vpc\_id | VPC ID | `string` | n/a | yes |
| vpc\_security\_group\_ids | VPC Security Groups IDs | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| address | n/a |
| allocated\_storage | n/a |
| arn | n/a |
| availability\_zone | n/a |
| backup\_retention\_period | n/a |
| backup\_window | n/a |
| ca\_cert\_identifier | n/a |
| domain | n/a |
| domain\_iam\_role\_name | n/a |
| endpoint | n/a |
| engine | n/a |
| engine\_version | n/a |
| hosted\_zone\_id | n/a |
| id | n/a |
| instance\_class | n/a |
| maintenance\_window | n/a |
| multi\_az | n/a |
| name | n/a |
| port | n/a |
| resource\_id | n/a |
| security\_group\_ids | n/a |
| security\_groups | n/a |
| sns\_arn | n/a |
| sns\_id | n/a |
| status | n/a |
| storage\_encrypted | n/a |
| username | n/a |

