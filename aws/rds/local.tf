locals {
  domain_slug = replace(var.domain, ".", "-")

  /* Default MSSQL Options
  mssql_options_group = [
    {
      option_name = "SQLSERVER_BACKUP_RESTORE"

      option_settings = [
        {
          name  = "IAM_ROLE_ARN"
          value = aws_iam_role.rds.arn
        }
      ]
    },
    {
      option_name = "SQLSERVER_AUDIT"

      option_settings = [
        {
          name  = "S3_BUCKET_ARN"
          value = "${var.bucket_arn}/mssql/audit"
        },
        {
          name  = "ENABLE_COMPRESSION"
          value = true
        },
        {
          name  = "IAM_ROLE_ARN"
          value = aws_iam_role.rds.arn
        },
        {
          name  = "RETENTION_TIME"
          value = var.retention_time
        }
      ]
    }
  ]
  */
}
