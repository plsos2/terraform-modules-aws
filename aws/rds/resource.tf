resource "aws_security_group" "database" {
  description = "Allows RDS Database Access"
  name_prefix = "rds-${var.database_identifier}-"
  vpc_id      = var.vpc_id

  tags = {
    Name    = var.domain
    Project = var.project_name
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
  }

  ingress {
    cidr_blocks = length(var.allowed_hosts) == 0 ? ["0.0.0.0/0"] : var.allowed_hosts
    protocol    = "tcp"
    from_port   = var.database_port
    to_port     = var.database_port
  }

  ingress {
    protocol        = "tcp"
    from_port       = var.database_port
    to_port         = var.database_port
    security_groups = var.security_groups
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_parameter_group" "default" {
  name        = "rds-parameters-${var.database_identifier}"
  description = "RDS Parameter Group"
  family      = "mysql5.7"

  dynamic "parameter" {
    iterator = parameter
    for_each = var.parameters

    content {
      apply_method = lookup(parameter.value, "apply_method", "immediate")
      name         = lookup(parameter.value, "name", null)
      value        = lookup(parameter.value, "value", null)
    }
  }
}

resource "aws_db_option_group" "default" {
  name                     = "rds-options-${var.database_identifier}"
  option_group_description = "RDS Option Group"
  engine_name              = var.engine
  major_engine_version     = var.major_engine_version

  dynamic "option" {
    iterator = option
    for_each = var.options

    content {
      option_name                    = option.value.option_name
      port                           = lookup(option.value, "port", null)
      version                        = lookup(option.value, "version", null)
      db_security_group_memberships  = lookup(option.value, "db_security_group_memberships", null)
      vpc_security_group_memberships = lookup(option.value, "vpc_security_group_memberships", null)

      dynamic "option_settings" {
        for_each = lookup(option.value, "option_settings", [])
        content {
          name  = lookup(option_settings.value, "name", null)
          value = lookup(option_settings.value, "value", null)
        }
      }
    }
  }
}

resource "random_id" "snapshot_id" {
  byte_length = 16
}

resource "aws_db_instance" "database" {
  allocated_storage                     = var.disk_size
  allow_major_version_upgrade           = var.allow_major_version_upgrade
  apply_immediately                     = var.apply_immediately
  auto_minor_version_upgrade            = var.auto_minor_version_upgrade
  availability_zone                     = var.availability_zone
  backup_retention_period               = var.backup_retention_period
  backup_window                         = var.backup_window
  character_set_name                    = var.character_set_name
  copy_tags_to_snapshot                 = var.copy_tags_to_snapshot
  engine                                = var.engine
  engine_version                        = var.engine_version
  db_subnet_group_name                  = var.db_subnet_group_name
  final_snapshot_identifier             = "final-${var.database_identifier}-${random_id.snapshot_id.hex}"
  iam_database_authentication_enabled   = var.iam_database_authentication_enabled
  identifier                            = var.database_identifier
  iops                                  = var.iops
  instance_class                        = var.instance_class
  kms_key_id                            = var.kms_key_id
  license_model                         = var.license_model
  maintenance_window                    = var.maintenance_window
  monitoring_interval                   = var.monitoring_interval
  monitoring_role_arn                   = var.enable_monitoring ? aws_iam_role.enhanced_monitoring[0].arn : var.monitoring_role_arn
  multi_az                              = var.multi_az
  name                                  = var.database_name
  option_group_name                     = aws_db_option_group.default.id
  password                              = var.password
  parameter_group_name                  = aws_db_parameter_group.default.id
  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = var.performance_insights_retention_period
  publicly_accessible                   = var.publicly_accessible
  skip_final_snapshot                   = var.skip_final_snapshot
  snapshot_identifier                   = var.snapshot_identifier
  storage_encrypted                     = var.storage_encrypted
  storage_type                          = var.storage_type
  username                              = var.username
  vpc_security_group_ids                = concat([aws_security_group.database.id], var.vpc_security_group_ids)
}

resource "aws_cloudwatch_log_group" "log" {
  name              = "/rds/${var.domain}/${var.database_identifier}"
  retention_in_days = var.retention_in_days

  tags = {
    Name    = var.domain
    Project = var.project_name
  }
}
