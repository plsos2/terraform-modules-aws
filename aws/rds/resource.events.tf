resource "aws_sns_topic" "rds" {
  name = "rds-events-${local.domain_slug}"

  tags = {
    Name    = var.domain
    Project = var.project_name
  }
}

resource "aws_db_event_subscription" "rds" {
  event_categories = var.event_categories
  name             = "rds-subscription-${local.domain_slug}"
  sns_topic        = aws_sns_topic.rds.arn
  source_ids       = [aws_db_instance.database.id]
  source_type      = "db-instance"


  tags = {
    Name    = var.domain
    Project = var.project_name
  }
}
