resource "aws_sqs_queue" "sqs_queue" {
  count                       = length(var.sqs_queues)
  name                        = var.sqs_queues[count.index].name
  fifo_queue                  = var.sqs_queues[count.index].fifo_queue
  content_based_deduplication = var.sqs_queues[count.index].content_based_deduplication
}