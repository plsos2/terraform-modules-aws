
variable "sqs_queues" {
  description = "An array of SQS Queues that the Lambda will access. This will create the sqs resource."
  type = list(object({
    name                        = string
    fifo_queue                  = bool
    content_based_deduplication = bool
  }))
}