## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| sqs\_queues | An array of SQS Queues that the Lambda will access. This will create the sqs resource. | <pre>list(object({<br>    name                        = string<br>    fifo_queue                  = bool<br>    content_based_deduplication = bool<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| sqs\_queues | n/a |

