output "availability_zones" {
  value = [for subnet in data.aws_subnet.subnets : subnet.availability_zone]
}

output "public_availability_zones" {
  value = [for subnet in data.aws_subnet.public : subnet.availability_zone]
}

output "private_availability_zones" {
  value = [for subnet in data.aws_subnet.private : subnet.availability_zone]
}

output "cidr_block" {
  value = data.aws_vpc.vpc.cidr_block
}

output "subnets" {
  value = [for subnet in data.aws_subnet.subnets : subnet.id]
}

output "private" {
  value = [for subnet in data.aws_subnet.private : subnet.id]
}

output "public" {
  value = [for subnet in data.aws_subnet.public : subnet.id]
}

output "vpc_id" {
  value = data.aws_vpc.vpc.id
}
