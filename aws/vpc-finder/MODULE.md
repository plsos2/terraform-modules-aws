## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| environment | Environment Name | `string` | n/a | yes |
| vpc\_name | VPC Name | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| availability\_zones | n/a |
| cidr\_block | n/a |
| private | n/a |
| private\_availability\_zones | n/a |
| public | n/a |
| public\_availability\_zones | n/a |
| subnets | n/a |
| vpc\_id | n/a |

