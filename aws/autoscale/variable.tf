variable "ami" {
  description = "AMI"
  type        = string
}

variable "domain" {
  description = "Domain Name"
  type        = string
}

variable "key_name" {
  description = "Key Name"
  type        = string
}

variable "name" {
  description = "Auto-Scaling Group name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "region" {
  description = "AWS Region"
  type        = string
}

variable "subnets" {
  description = "Subnets"
  type        = list(string)
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}
