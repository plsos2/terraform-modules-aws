variable "allowed_hosts" {
  default     = []
  description = "Allowed Hosts"
  type        = list(string)
}

variable "associate_public_ip_address" {
  default     = true
  description = "Associate a public IP address"
  type        = bool
}

variable "capacity" {
  default     = 1
  description = "Number of machines desired"
  type        = number
}

variable "capacity_max" {
  default     = 1
  description = "Maximum number of VMs to create"
  type        = number
}

variable "capacity_min" {
  default     = 1
  description = "Minimum number of VMs to create"
  type        = number
}

variable "enable_monitoring" {
  default     = true
  description = "Enable monitoring"
  type        = bool
}

variable "health_check_type" {
  default     = "ELB"
  description = "Health Check Type"
  type        = string
}

variable "ingress" {
  /**
    There is an order of precendence here. The rules are:
    1) If source_cidr_blocks is valid, use that [source_cidr_blocks]
    2) If source_cidr_blocks is empty and source_security_groups is set, use [source_security_groups]
    3) If source_cidr_blocks and source_security_groups are empty, use [allowed_hosts]
  */
  default = [{
    description            = "HTTP"
    port                   = 80
    protocol               = "tcp"
    source_cidr_blocks     = ["0.0.0.0/0"]
    source_security_groups = []
    }, {
    description            = "HTTPS"
    port                   = 443
    protocol               = "tcp"
    source_cidr_blocks     = ["0.0.0.0/0"]
    source_security_groups = []
    }, {
    description            = "SSH"
    port                   = 22
    protocol               = "tcp"
    source_cidr_blocks     = []
    source_security_groups = []
  }]
  description = "Ingress Ports"
  type = list(object({
    description            = string
    port                   = number
    protocol               = string
    source_cidr_blocks     = list(string)
    source_security_groups = list(string)
  }))
}

variable "instance_type" {
  default     = "t2.micro"
  description = "AWS Instance Type"
  type        = string
}

variable "security_groups" {
  default     = []
  description = "Security groups for instances"
  type        = list(string)
}

variable "target_port" {
  default     = 80
  description = "Target Group Port"
  type        = number
}

variable "target_protocol" {
  default     = "HTTP"
  description = "Target Group Protocol"
  type        = string
}

variable "target_health_path" {
  default     = "/"
  description = "Target Group health check path"
  type        = string
}

variable "target_health_port" {
  default     = 80
  description = "Target Group health check port"
  type        = number
}

variable "termination_policies" {
  default     = ["OldestInstance"]
  description = "Termination Policies"
  type        = list(string)
}

variable "user_data" {
  default     = null
  description = "User Data"
  type        = string
}

variable "volume_delete_on_termination" {
  default     = true
  description = "Delete volume when instance is terminated"
  type        = bool
}

variable "volume_iops" {
  default     = 0
  description = "Volume IOPS"
  type        = number
}

variable "volume_size" {
  default     = 64
  description = "Volume size"
  type        = number
}

variable "volume_type" {
  default     = "gp2"
  description = "Volume type"
  type        = string
}

variable "wait_for_elb_capacity" {
  default     = 0
  description = "Waits for the load balancers to appear healthy as well"
  type        = number
}
