## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allowed\_hosts | Allowed Hosts | `list(string)` | `[]` | no |
| ami | AMI | `string` | n/a | yes |
| associate\_public\_ip\_address | Associate a public IP address | `bool` | `true` | no |
| capacity | Number of machines desired | `number` | `1` | no |
| capacity\_max | Maximum number of VMs to create | `number` | `1` | no |
| capacity\_min | Minimum number of VMs to create | `number` | `1` | no |
| domain | Domain Name | `string` | n/a | yes |
| enable\_monitoring | Enable monitoring | `bool` | `true` | no |
| health\_check\_type | Health Check Type | `string` | `"ELB"` | no |
| ingress | Ingress Ports | <pre>list(object({<br>    description            = string<br>    port                   = number<br>    protocol               = string<br>    source_cidr_blocks     = list(string)<br>    source_security_groups = list(string)<br>  }))</pre> | <pre>[<br>  {<br>    "description": "HTTP",<br>    "port": 80,<br>    "protocol": "tcp",<br>    "source_cidr_blocks": [<br>      "0.0.0.0/0"<br>    ],<br>    "source_security_groups": []<br>  },<br>  {<br>    "description": "HTTPS",<br>    "port": 443,<br>    "protocol": "tcp",<br>    "source_cidr_blocks": [<br>      "0.0.0.0/0"<br>    ],<br>    "source_security_groups": []<br>  },<br>  {<br>    "description": "SSH",<br>    "port": 22,<br>    "protocol": "tcp",<br>    "source_cidr_blocks": [],<br>    "source_security_groups": []<br>  }<br>]</pre> | no |
| instance\_type | AWS Instance Type | `string` | `"t2.micro"` | no |
| key\_name | Key Name | `string` | n/a | yes |
| name | Auto-Scaling Group name | `string` | n/a | yes |
| project\_name | Project Name | `string` | n/a | yes |
| region | AWS Region | `string` | n/a | yes |
| security\_groups | Security groups for instances | `list(string)` | `[]` | no |
| subnets | Subnets | `list(string)` | n/a | yes |
| target\_health\_path | Target Group health check path | `string` | `"/"` | no |
| target\_health\_port | Target Group health check port | `number` | `80` | no |
| target\_port | Target Group Port | `number` | `80` | no |
| target\_protocol | Target Group Protocol | `string` | `"HTTP"` | no |
| termination\_policies | Termination Policies | `list(string)` | <pre>[<br>  "OldestInstance"<br>]</pre> | no |
| user\_data | User Data | `string` | `null` | no |
| volume\_delete\_on\_termination | Delete volume when instance is terminated | `bool` | `true` | no |
| volume\_iops | Volume IOPS | `number` | `0` | no |
| volume\_size | Volume size | `number` | `64` | no |
| volume\_type | Volume type | `string` | `"gp2"` | no |
| vpc\_id | VPC ID | `string` | n/a | yes |
| wait\_for\_elb\_capacity | Waits for the load balancers to appear healthy as well | `number` | `0` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_autoscaling\_group\_id | n/a |
| security\_group\_arns | n/a |
| security\_group\_ids | n/a |
| security\_group\_names | n/a |
| target\_group\_arn | n/a |

