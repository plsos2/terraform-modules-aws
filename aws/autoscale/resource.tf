data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "role" {
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
  name               = "autoscale-role-${var.domain}"
}

resource "aws_iam_instance_profile" "profile" {
  name = "autoscale-profile-${var.domain}"
  role = aws_iam_role.role.name
}

resource "aws_launch_configuration" "configuration" {
  associate_public_ip_address = var.associate_public_ip_address
  enable_monitoring           = var.enable_monitoring
  iam_instance_profile        = aws_iam_instance_profile.profile.arn
  image_id                    = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  name_prefix                 = "${var.domain}-"
  security_groups             = concat(var.security_groups, [aws_security_group.instance.id])
  user_data                   = var.user_data

  lifecycle {
    create_before_destroy = true
  }

  root_block_device {
    delete_on_termination = var.volume_delete_on_termination
    iops                  = var.volume_iops
    volume_size           = var.volume_size
    volume_type           = var.volume_type
  }
}

resource "aws_autoscaling_group" "autoscaling" {
  depends_on                = [aws_lb_target_group.target_group, aws_launch_configuration.configuration]
  desired_capacity          = var.capacity
  force_delete              = false
  health_check_grace_period = 180
  health_check_type         = var.health_check_type
  launch_configuration      = aws_launch_configuration.configuration.name
  max_size                  = var.capacity_max
  min_size                  = var.capacity_min
  name                      = "autoscaling-group-${var.domain}"
  target_group_arns         = [aws_lb_target_group.target_group.arn]
  termination_policies      = var.termination_policies
  vpc_zone_identifier       = var.subnets
  wait_for_elb_capacity     = var.wait_for_elb_capacity
}

resource "aws_lb_target_group" "target_group" {
  port     = var.target_port
  protocol = var.target_protocol
  vpc_id   = var.vpc_id

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = var.target_health_path
    port                = var.target_health_port
  }

  tags = {
    Name    = var.domain
    Project = var.project_name
  }
}

resource "aws_security_group" "instance" {
  name_prefix = "autoscale-${var.domain}-"
  description = "Allows public traffic over specific ports."
  vpc_id      = var.vpc_id

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
  }

  dynamic "ingress" {
    for_each = range(0, length(var.ingress))

    content {
      description = var.ingress[ingress.value].description
      from_port   = var.ingress[ingress.value].port
      to_port     = var.ingress[ingress.value].port
      protocol    = var.ingress[ingress.value].protocol
      // This is some crazy sh*t right here... - MP
      cidr_blocks     = length(var.ingress[ingress.value].source_cidr_blocks) + length(var.ingress[ingress.value].source_security_groups) == 0 ? var.allowed_hosts : var.ingress[ingress.value].source_cidr_blocks
      security_groups = length(var.ingress[ingress.value].source_cidr_blocks) == 0 ? var.ingress[ingress.value].source_security_groups : []
    }
  }

  tags = {
    Name    = var.domain
    Project = var.project_name
  }

  lifecycle {
    create_before_destroy = true
  }
}
