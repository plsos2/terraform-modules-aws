output "aws_autoscaling_group_id" {
  value = aws_autoscaling_group.autoscaling.id
}

output "security_group_arns" {
  value = [aws_security_group.instance.arn]
}

output "security_group_ids" {
  value = [aws_security_group.instance.id]
}

output "security_group_names" {
  value = [aws_security_group.instance.name]
}

output "target_group_arn" {
  value = aws_lb_target_group.target_group.arn
}
