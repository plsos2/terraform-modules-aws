variable "bucket_name" {
  description = "Bucket name"
  type        = string
}

variable "domain" {
  description = "Domain name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}
