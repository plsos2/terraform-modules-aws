## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |
| local | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| algorithm | SSH KeyPair algorithm | `string` | `"RSA"` | no |
| bucket\_name | Bucket name | `string` | n/a | yes |
| domain | Domain name | `string` | n/a | yes |
| project\_name | Project Name | `string` | n/a | yes |
| rsa\_bits | Length of RSA KeyPair | `string` | `4096` | no |

## Outputs

| Name | Description |
|------|-------------|
| key\_name | n/a |
| key\_path | n/a |
| kms\_key\_arn | n/a |
| kms\_key\_id | n/a |
| private\_key\_pem | n/a |
| public\_key\_openssh | n/a |
| public\_key\_pem | n/a |

