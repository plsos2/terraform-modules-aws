## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| additional\_certificate\_arns | Additional Certificates | `list(string)` | `[]` | no |
| certificate\_arn | Description ARN | `string` | n/a | yes |
| domain | Domain name | `string` | n/a | yes |
| enable\_deletion\_protection | Enable Deletion Protection | `bool` | `false` | no |
| listener\_port | Load balancer listening port | `string` | `80` | no |
| listener\_protocol | Load balancer listening protocol | `string` | `"HTTP"` | no |
| listener\_secure\_port | Load balancer listening secure port | `string` | `443` | no |
| listener\_secure\_protocol | Load balancer listening secure protocol | `string` | `"HTTPS"` | no |
| load\_balancer\_type | Load Balancer Type | `string` | `"application"` | no |
| project\_name | Project Name | `string` | n/a | yes |
| rules | Target groups to attach | <pre>list(<br>    object(<br>      {<br>        host_header      = string<br>        path_pattern     = string<br>        target_group_arn = string<br>        type             = string<br>      }<br>    )<br>  )</pre> | `[]` | no |
| security\_groups | AWS Security Groups | `list(string)` | `[]` | no |
| subnets | Subnets | `list(string)` | n/a | yes |
| target\_group\_arn | Target group to attach | `string` | n/a | yes |
| vpc\_id | VPC | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn | n/a |
| arn\_suffix | n/a |
| dns\_name | n/a |
| id | n/a |
| name | n/a |
| security\_group | n/a |
| zone\_id | n/a |

