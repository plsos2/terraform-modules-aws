## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| domain | Domain Name | `string` | n/a | yes |
| enable\_ses | Enable SES | `bool` | n/a | yes |
| profile | Profile | `string` | n/a | yes |
| project\_name | Project Name | `string` | n/a | yes |
| region | AWS Region | `string` | n/a | yes |
| username | SES Username | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| access\_key | n/a |
| encrypted\_secret | n/a |
| secret\_key | n/a |
| ses\_smtp\_password | n/a |

