data "aws_route53_zone" "zone" {
  count = var.enable_ses ? 1 : 0
  name  = "${var.domain}."
}

resource "aws_ses_domain_identity" "identity" {
  count  = var.enable_ses ? 1 : 0
  domain = var.domain
}

resource "aws_route53_record" "verification" {
  count   = var.enable_ses ? 1 : 0
  name    = "_amazonses.${var.domain}"
  records = aws_ses_domain_identity.identity[0].*.verification_token
  ttl     = 600
  type    = "TXT"
  zone_id = data.aws_route53_zone.zone[0].zone_id
}

resource "aws_ses_domain_identity_verification" "verification" {
  count      = var.enable_ses ? 1 : 0
  depends_on = [aws_route53_record.verification]
  domain     = aws_ses_domain_identity.identity[0].id
}

resource "aws_ses_domain_dkim" "dkim" {
  count  = var.enable_ses ? 1 : 0
  domain = aws_ses_domain_identity.identity[0].domain
}

resource "aws_route53_record" "dkim" {
  count   = var.enable_ses ? 3 : 0
  name    = "${element(aws_ses_domain_dkim.dkim[0].dkim_tokens, count.index)}._domainkey.${var.domain}"
  records = ["${element(aws_ses_domain_dkim.dkim[0].dkim_tokens, count.index)}.dkim.amazonses.com"]
  ttl     = 600
  type    = "CNAME"
  zone_id = data.aws_route53_zone.zone[0].zone_id
}

resource "aws_ses_domain_mail_from" "ses_domain_mail" {
  count            = var.enable_ses ? 1 : 0
  domain           = aws_ses_domain_identity.identity[0].domain
  mail_from_domain = "bounce.${aws_ses_domain_identity.identity[0].domain}"
}

resource "aws_route53_record" "ses_domain_max" {
  count   = var.enable_ses ? 1 : 0
  name    = aws_ses_domain_mail_from.ses_domain_mail[0].mail_from_domain
  records = ["10 feedback-smtp.${var.region}.amazonses.com"]
  ttl     = 600
  type    = "MX"
  zone_id = data.aws_route53_zone.zone[0].id
}

resource "aws_route53_record" "ses_domain_txt" {
  count   = var.enable_ses ? 1 : 0
  name    = aws_ses_domain_mail_from.ses_domain_mail[0].mail_from_domain
  records = ["v=spf1 include:amazonses.com -all"]
  ttl     = 600
  type    = "TXT"
  zone_id = data.aws_route53_zone.zone[0].id
}

resource "aws_iam_user" "ses" {
  count = var.enable_ses ? 1 : 0
  name  = coalesce(var.username, "ses.${var.domain}")
}

resource "aws_iam_access_key" "ses" {
  count = var.enable_ses ? 1 : 0
  user  = aws_iam_user.ses[0].name
}
