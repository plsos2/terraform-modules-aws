output "access_key" {
  value = aws_iam_access_key.ses.*.id
}

output "encrypted_secret" {
  value = aws_iam_access_key.ses.*.encrypted_secret
}

output "secret_key" {
  value = aws_iam_access_key.ses.*.secret
}

output "ses_smtp_password" {
  value = aws_iam_access_key.ses.*.ses_smtp_password
}
