variable "domain" {
  description = "Domain Name"
  type        = string
}

variable "enable_ses" {
  description = "Enable SES"
  type        = bool
}

variable "profile" {
  description = "Profile"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "username" {
  default     = null
  description = "SES Username"
  type        = string
}

variable "region" {
  description = "AWS Region"
  type        = string
}
