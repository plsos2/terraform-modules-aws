output "permissions_arn" {
  value = var.enabled ? aws_iam_policy.permissions[*].arn : []
}

output "role_ecs" {
  value = aws_iam_role.ecs
}

output "role_ecs_task" {
  value = aws_iam_role.task
}

output "port" {
  value = var.container_port
}

output "security_groups" {
  value = local.security_groups
}

output "target_group_arns" {
  value = var.enabled ? aws_lb_target_group.ecs[*].arn : []
}

output "rules" {
  value = var.enabled ? [
    for index, target in aws_lb_target_group.ecs :
    merge({ target_group_arn = target.arn }, { host_header = [var.containers[index].dns_name] })
  ] : []
}
