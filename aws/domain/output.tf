output "domain_name" {
  value = var.domain
}

output "zone_id" {
  value = data.aws_route53_zone.zone.id
}
