## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| domain | Zone record for domain | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| domain\_name | n/a |
| zone\_id | n/a |

