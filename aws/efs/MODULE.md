## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| domain | Domain name | `string` | n/a | yes |
| project\_name | Project Name | `string` | n/a | yes |
| subnets | Subnets to attach EFS | `list(string)` | n/a | yes |
| vpc\_id | VPC | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn | EFS ARN |
| dns\_name | EFS DNS Name |
| efs\_arn | OBSOLETE |
| efs\_dns\_name | OBSOLETE |
| efs\_id | OBSOLETE |
| efs\_security\_group | OBSOLETE |
| id | EFS ID |
| security\_groups | n/a |

