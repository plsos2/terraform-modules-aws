## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| environment\_variables | The environment variables that you want to pass into the Lambda function. | `map(string)` | n/a | yes |
| function\_name | The name of the actual Lambda function | `string` | n/a | yes |
| handler | The name of the handler for your lambda. The function entrypoint in your code. | `string` | n/a | yes |
| layers | The Layers that the Lambda Function will be running on. | `list(string)` | n/a | yes |
| name | A name for our Lambda and its associated resources | `string` | n/a | yes |
| path | A path that leads to the zip file for the lambda within the current project. Example - ../src/PropLogix.Security.Access.HRAdapter.Listener/Listener.zip | `string` | n/a | yes |
| policies | IAM Policy ARNs to be attached to role | `list` | n/a | yes |
| runtime | The runtime for your Lambda. Valid values - nodejs10.x \| nodejs12.x \| java8 \| java11 \| python2.7 \| python3.6 \| python3.7 \| python3.8 \| dotnetcore2.1 \| go1.x \| ruby2.5 \| ruby2.7 \| provided | `string` | n/a | yes |
| sns\_topics | A list of SNS Topics that have been created and will be subscribed to by the Lambda Function. | <pre>list(object({<br>    arn    = string<br>    id     = string<br>    name   = string<br>    policy = string<br>  }))</pre> | n/a | yes |
| xray\_enabled | Toggle for enabling x-ray tracing | `bool` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| lambda | n/a |

