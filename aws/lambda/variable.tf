variable "name" {
  description = "A name for our Lambda and its associated resources"
  type        = string
}

variable "function_name" {
  description = "The name of the actual Lambda function"
  type        = string
}

variable "sns_topics" {
  description = "A list of SNS Topics that have been created and will be subscribed to by the Lambda Function."
  type = list(object({
    arn    = string
    id     = string
    name   = string
    policy = string
  }))
}

variable "path" {
  description = "A path that leads to the zip file for the lambda within the current project. Example - ../src/PropLogix.Security.Access.HRAdapter.Listener/Listener.zip"
  type        = string
}

variable "handler" {
  description = "The name of the handler for your lambda. The function entrypoint in your code."
  type        = string
}

variable "runtime" {
  description = "The runtime for your Lambda. Valid values - nodejs10.x | nodejs12.x | java8 | java11 | python2.7 | python3.6 | python3.7 | python3.8 | dotnetcore2.1 | go1.x | ruby2.5 | ruby2.7 | provided"
  type        = string
}

variable "xray_enabled" {
  description = "Toggle for enabling x-ray tracing"
  type        = bool
}

variable "policies" {
  description = "IAM Policy ARNs to be attached to role"   
  type        = list
}

variable "layers" {
  description = "The Layers that the Lambda Function will be running on."
  type        = list(string)
}

variable "environment_variables" {
  description = "The environment variables that you want to pass into the Lambda function."
  type        = map(string)
}


