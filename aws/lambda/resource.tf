resource "aws_iam_role" "lambda_role" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.lambda.json
}

data "aws_iam_policy_document" "lambda" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role_policy_attachment" "iam_policy_for_lambda" {
  count  = length(var.policies)
  role   = aws_iam_role.lambda_role.id
  policy_arn = var.policies[count.index]
}

resource "aws_iam_role_policy_attachment" "x_ray_attach" {
  count      = var.xray_enabled ? 1 : 0
  role       = aws_iam_role.lambda_role.id
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
}

resource "aws_lambda_function" "lambda_function" {
  filename         = var.path
  function_name    = var.function_name
  role             = aws_iam_role.lambda_role.arn
  handler          = var.handler
  source_code_hash = filebase64sha256(var.path)
  runtime          = var.runtime
  layers           = var.layers
  dynamic "environment" {
    for_each = var.environment_variables == null ? [] : [var.environment_variables]
    content {
      variables = var.environment_variables
    }
  }
  tracing_config {
    mode = var.xray_enabled ? "Active" : "PassThrough"
  }
}


resource "aws_lambda_permission" "sns_permission" {
  count         = length(var.sns_topics)
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.arn
  principal     = "sns.amazonaws.com"
  source_arn    = var.sns_topics[count.index].arn
}


resource "aws_sns_topic_subscription" "sns_subscription" {
  count     = length(var.sns_topics)
  topic_arn = var.sns_topics[count.index].arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.lambda_function.arn
}
