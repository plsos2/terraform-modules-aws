variable "cluster_name" {
  description = "Cluster Name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}
