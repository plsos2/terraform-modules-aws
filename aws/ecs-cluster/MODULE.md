## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| cluster\_name | Cluster Name | `string` | n/a | yes |
| enable\_container\_insights | Enable Container Insights | `bool` | `true` | no |
| project\_name | Project Name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_id | n/a |

