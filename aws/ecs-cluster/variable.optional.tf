variable "enable_container_insights" {
  default     = true
  description = "Enable Container Insights"
  type        = bool
}
