## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| domain | Domain Name | `string` | n/a | yes |
| group\_name | Group Name | `string` | n/a | yes |
| policies | Policies | `list(string)` | <pre>[<br>  "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",<br>  "arn:aws:iam::aws:policy/AmazonS3FullAccess",<br>  "arn:aws:iam::aws:policy/SecretsManagerReadWrite",<br>  "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"<br>]</pre> | no |
| project\_name | Project Name | `string` | n/a | yes |
| user\_name | User Name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| access\_key | n/a |
| arn | n/a |
| id | n/a |
| secret\_key | n/a |

