variable "domain" {
  description = "Domain Name"
  type        = string
}

variable "group_name" {
  description = "Group Name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "user_name" {
  description = "User Name"
  type        = string
}
