## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allowed\_hosts | Allowed Hosts | `list(string)` | n/a | yes |
| availability\_zones | Availablity Aones | `list(string)` | n/a | yes |
| certificate\_arn | Description ARN | `string` | n/a | yes |
| cluster\_name | Cluster Name | `string` | `null` | no |
| container\_port | Container Service Port | `number` | `80` | no |
| container\_port\_ssl | Container Service Port | `number` | `443` | no |
| containers | Container Services | <pre>list(object({<br>    cpu                      = number<br>    definitions              = string<br>    desired_count            = number<br>    dns_name                 = string<br>    family                   = string<br>    health_check_path        = string<br>    launch_type              = string<br>    log_name                 = string<br>    memory                   = number<br>    name                     = string<br>    network_mode             = string<br>    port                     = number<br>    public_ip                = bool<br>    requires_compatibilities = list(string)<br>    security_groups          = list(string)<br>  }))</pre> | n/a | yes |
| database\_port | Database Port | `number` | `3306` | no |
| deployment\_controller\_type | Deployment Controller Type | `string` | `"ECS"` | no |
| deployment\_maximum\_percent | Deployment Maximum Percent | `number` | `null` | no |
| deployment\_minimum\_healthy\_percent | Deployment Minimum Healthy Percent | `number` | `null` | no |
| desired\_count | Desired Instance Count | `number` | `1` | no |
| domain | Domain name | `string` | n/a | yes |
| enable\_container\_insights | Enable Container Insights | `bool` | `true` | no |
| enable\_deletion\_protection | Enable Deletion Protection | `bool` | `false` | no |
| enable\_ecs\_managed\_tags | Enable ECS Managed Tags | `bool` | `false` | no |
| iam\_role | IAM Role | `string` | `null` | no |
| load\_balancer\_type | Load Balancer Type | `string` | `"application"` | no |
| module\_depends\_on | n/a | `any` | `null` | no |
| platform\_version | Platform Version | `string` | `"LATEST"` | no |
| policies | Policies | `list(string)` | <pre>[<br>  "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",<br>  "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",<br>  "arn:aws:iam::aws:policy/SecretsManagerReadWrite"<br>]</pre> | no |
| project\_name | Project Name | `string` | n/a | yes |
| propagate\_tags | Propogate Tags | `string` | `null` | no |
| retention\_in\_days | Retention in Days | `number` | `90` | no |
| secret\_resources | Secret Resources | `list(string)` | `[]` | no |
| security\_group\_ids | Security Group IDs | `list(string)` | `[]` | no |
| subnets | Subnets | `list(string)` | n/a | yes |
| vpc\_id | VPC ID | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_id | n/a |
| permissions\_arn | n/a |
| port | n/a |
| role\_ecs\_arn | n/a |
| role\_ecs\_id | n/a |
| role\_ecs\_task\_arn | n/a |
| role\_ecs\_task\_id | n/a |
| rules | n/a |
| security\_groups | n/a |
| target\_group\_arns | n/a |

