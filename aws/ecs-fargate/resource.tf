data "aws_iam_policy_document" "ecs" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["ecs-tasks.amazonaws.com", "iam.amazonaws.com", "s3.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "permissions" {
  statement {
    actions = [
      "iam:GetUser",
      "secretsmanager:GetSecretValue",
    ]
    effect    = "Allow"
    resources = var.secret_resources
  }
}

resource "aws_iam_policy" "permissions" {
  description = "ECS Permissions"
  name_prefix = "${var.cluster_name}-"
  policy      = data.aws_iam_policy_document.permissions.json
}

resource "aws_iam_role" "ecs" {
  assume_role_policy = data.aws_iam_policy_document.ecs.json
  name               = "role-ecs-${replace(var.domain, ".", "-")}"
}

resource "aws_iam_role" "task" {
  assume_role_policy = data.aws_iam_policy_document.ecs.json
  name               = "role-task-${replace(var.domain, ".", "-")}"
}

resource "aws_iam_role_policy_attachment" "ecs" {
  count      = length(var.policies)
  role       = aws_iam_role.ecs.name
  policy_arn = var.policies[count.index]
}

resource "aws_iam_role_policy_attachment" "task" {
  count      = length(var.policies)
  role       = aws_iam_role.task.name
  policy_arn = var.policies[count.index]
}

resource "aws_security_group" "containers" {
  description = "Allow ECS Service Access"
  name_prefix = "${var.cluster_name}-"
  vpc_id      = var.vpc_id

  tags = {
    Name    = var.domain
    Project = var.project_name
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
  }

  ingress {
    protocol        = "tcp"
    from_port       = var.database_port
    to_port         = var.database_port
    security_groups = [aws_security_group.tasks.id]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "tasks" {
  description = "Allow ECS Task Access"
  name_prefix = "${var.cluster_name}-"
  vpc_id      = var.vpc_id

  tags = {
    Name    = var.domain
    Project = var.project_name
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = var.container_port
    to_port     = var.container_port
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = var.container_port_ssl
    to_port     = var.container_port_ssl
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_target_group" "ecs" {
  count       = length(var.containers)
  name_prefix = "tgt-"
  port        = var.containers[count.index].port
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id
  depends_on  = [var.module_depends_on]

  health_check {
    path = var.containers[count.index].health_check_path
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_ecs_cluster" "ecs" {
  name = coalesce(var.cluster_name, var.domain)

  tags = {
    Name    = var.domain
    Project = var.project_name
  }

  setting {
    name  = "containerInsights"
    value = var.enable_container_insights ? "enabled" : "disabled"
  }
}

resource "aws_ecs_task_definition" "ecs" {
  count                    = length(var.containers)
  container_definitions    = var.containers[count.index].definitions
  cpu                      = var.containers[count.index].cpu
  execution_role_arn       = aws_iam_role.ecs.arn
  memory                   = var.containers[count.index].memory
  family                   = var.containers[count.index].family
  network_mode             = var.containers[count.index].network_mode
  requires_compatibilities = var.containers[count.index].requires_compatibilities
  task_role_arn            = aws_iam_role.task.arn
}

resource "aws_ecs_service" "ecs" {
  count                              = length(var.containers)
  cluster                            = aws_ecs_cluster.ecs.id
  deployment_maximum_percent         = var.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
  desired_count                      = var.containers[count.index].desired_count
  enable_ecs_managed_tags            = var.enable_ecs_managed_tags
  iam_role                           = var.iam_role
  launch_type                        = var.containers[count.index].launch_type
  propagate_tags                     = var.propagate_tags
  name                               = var.containers[count.index].name
  platform_version                   = var.platform_version
  task_definition                    = aws_ecs_task_definition.ecs[count.index].arn

  deployment_controller {
    type = var.deployment_controller_type
  }

  lifecycle {
    ignore_changes = [task_definition]
  }

  load_balancer {
    container_name   = var.containers[count.index].name
    container_port   = var.containers[count.index].port
    target_group_arn = aws_lb_target_group.ecs[count.index].arn
  }

  network_configuration {
    assign_public_ip = var.containers[count.index].public_ip
    security_groups  = length(var.containers[count.index].security_groups) == 0 ? [aws_security_group.tasks.id] : var.containers[count.index].security_groups
    subnets          = var.subnets
  }
}

resource "aws_cloudwatch_log_group" "log" {
  count             = length(var.containers)
  name              = var.containers[count.index].log_name
  retention_in_days = var.retention_in_days

  tags = {
    Container = var.containers[count.index].name
    Name      = var.domain
    Project   = var.project_name
  }
}
