variable "allowed_hosts" {
  description = "Allowed Hosts"
  type        = list(string)
}

variable "availability_zones" {
  description = "Availablity Aones"
  type        = list(string)
}

variable "certificate_arn" {
  description = "Description ARN"
  type        = string
}

variable "containers" {
  description = "Container Services"
  type = list(object({
    cpu                      = number
    definitions              = string
    desired_count            = number
    dns_name                 = string
    family                   = string
    health_check_path        = string
    launch_type              = string
    log_name                 = string
    memory                   = number
    name                     = string
    network_mode             = string
    port                     = number
    public_ip                = bool
    requires_compatibilities = list(string)
    security_groups          = list(string)
  }))
}

variable "domain" {
  description = "Domain name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "subnets" {
  description = "Subnets"
  type        = list(string)
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}

variable "module_depends_on" {
  type    = any
  default = null
}