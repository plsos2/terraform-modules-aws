## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |
| random | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| domain | Domain Name | `string` | n/a | yes |
| project\_name | Project Name | `string` | n/a | yes |
| secret\_name | Secret Name | `string` | n/a | yes |
| secrets | n/a | `map` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn | n/a |
| secret | Environment Secrets |

