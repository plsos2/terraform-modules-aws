variable "allowed_hosts" {
  description = "List of IP addresses to allow access to the cluster"
  type        = list(string)
}

variable "domain" {
  description = "Domain name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "subnets" {
  description = "Subnets to attach EKS to"
  type        = list(string)
}

variable "vpc_id" {
  description = "VPC"
  type        = string
}
