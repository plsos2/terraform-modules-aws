## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allowed\_hosts | List of IP addresses to allow access to the cluster | `list(string)` | n/a | yes |
| domain | Domain name | `string` | n/a | yes |
| project\_name | Project Name | `string` | n/a | yes |
| subnets | Subnets to attach EKS to | `list(string)` | n/a | yes |
| vpc\_id | VPC | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| eks\_arns | EKS ARNs |

