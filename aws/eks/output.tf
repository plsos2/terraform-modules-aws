output "eks_arns" {
  description = "EKS ARNs"
  value       = aws_eks_cluster.eks.*.arn
}
