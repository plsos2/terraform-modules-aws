variable "domain_san" {
  default     = []
  description = "Certificate Subject Alternative Names"
  type        = list(string)
}

variable "validation_method" {
  default     = "DNS"
  description = "DNS Validation Method"
  type        = string
}
