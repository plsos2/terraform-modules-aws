# This is the supposed correct way to do this, but it currently doesn't work
# https://github.com/terraform-providers/terraform-provider-aws/issues/14447
# resource "aws_route53_record" "dns" {
#   for_each = {
#     for dvo in aws_acm_certificate.certificate.domain_validation_options : dvo.domain_name => {
#       name    = dvo.resource_record_name
#       record  = dvo.resource_record_value
#       type    = dvo.resource_record_type
#       zone_id = var.zone_id
#     }
#   }

#   allow_overwrite = true
#   name            = each.value.name
#   records         = [each.value.record]
#   ttl             = 60
#   type            = each.value.type
#   zone_id         = each.value.zone_id
# }

resource "aws_route53_record" "dns" {
  name            = aws_acm_certificate.certificate.domain_validation_options.*.resource_record_name[0]
  records         = [aws_acm_certificate.certificate.domain_validation_options.*.resource_record_value[0]]
  type            = aws_acm_certificate.certificate.domain_validation_options.*.resource_record_type[0]
  zone_id         = var.zone_id
  ttl             = 60
}

resource "aws_acm_certificate" "certificate" {
  domain_name               = var.domain
  subject_alternative_names = var.domain_san
  validation_method         = var.validation_method
}

resource "aws_acm_certificate_validation" "validaton" {
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = aws_route53_record.dns.*.fqdn
}
