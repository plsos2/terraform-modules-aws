variable "domain" {
  description = "Domain name"
  type        = string
}

variable "project_name" {
  description = "Project Name"
  type        = string
}

variable "zone_id" {
  description = "Zone Id"
  type        = string
}
