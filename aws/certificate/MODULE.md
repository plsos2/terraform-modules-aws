## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| domain | Domain name | `string` | n/a | yes |
| domain\_san | Certificate Subject Alternative Names | `list(string)` | `[]` | no |
| project\_name | Project Name | `string` | n/a | yes |
| validation\_method | DNS Validation Method | `string` | `"DNS"` | no |
| zone\_id | Zone Id | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| certificate\_arn | Certificate ARN |
| certificate\_id | Certificate ID |
| domain\_name | Domain Name |
| status | Certificate validation status |

