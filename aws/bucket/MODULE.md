## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 2.70 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.70 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| acl | ACL | `string` | `"private"` | no |
| bucket\_name | Bucket Name | `string` | n/a | yes |
| domain | Domain name | `string` | n/a | yes |
| force\_destroy | Force Destroy | `bool` | `false` | no |
| principal\_arns | Principal ARNs | `list(string)` | n/a | yes |
| project\_name | Project Name | `string` | n/a | yes |
| versioning\_enabled | Enable Versioning | `bool` | `true` | no |
| versioning\_mfa\_delete | Require MFA to Delete | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| arn | n/a |
| bucket\_domain\_name | n/a |
| bucket\_name | n/a |
| bucket\_regional\_domain\_name | n/a |
| hosted\_zone\_id | n/a |
| id | n/a |
| name | n/a |
| region | n/a |
| website\_domain | n/a |
| website\_endpoint | n/a |

