variable "domain" {
  description = "Domain name"
  type        = string
}

variable "bucket_name" {
  description = "Bucket Name"
  type        = string
}

variable "principal_arns" {
  description = "Principal ARNs"
  type        = list(string)
}

variable "project_name" {
  description = "Project Name"
  type        = string
}
