output "arn" {
  value = aws_s3_bucket.bucket.arn
}

output "bucket_name" {
  value = var.bucket_name
}

output "bucket_domain_name" {
  value = aws_s3_bucket.bucket.bucket_domain_name
}

output "bucket_regional_domain_name" {
  value = aws_s3_bucket.bucket.bucket_regional_domain_name
}

output "hosted_zone_id" {
  value = aws_s3_bucket.bucket.hosted_zone_id
}

output "id" {
  value = aws_s3_bucket.bucket.id
}

output "name" {
  value = var.bucket_name
}

output "region" {
  value = aws_s3_bucket.bucket.region
}

output "website_endpoint" {
  value = aws_s3_bucket.bucket.website_endpoint
}

output "website_domain" {
  value = aws_s3_bucket.bucket.website_domain
}
