variable "acl" {
  default     = "private"
  description = "ACL"
  type        = string
}

variable "force_destroy" {
  default     = false
  description = "Force Destroy"
  type        = bool
}

variable "versioning_enabled" {
  default     = true
  description = "Enable Versioning"
  type        = bool
}

variable "versioning_mfa_delete" {
  default     = false
  description = "Require MFA to Delete"
  type        = bool
}
