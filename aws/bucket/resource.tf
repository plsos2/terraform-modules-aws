data "aws_iam_policy_document" "policy" {
  statement {
    actions   = ["s3:*"]
    effect    = "Allow"
    resources = [aws_s3_bucket.bucket.arn]

    principals {
      identifiers = var.principal_arns
      type        = "AWS"
    }
  }
}

resource "aws_s3_bucket_policy" "policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.aws_iam_policy_document.policy.json
}

resource "aws_s3_bucket" "bucket" {
  acl           = var.acl
  bucket        = coalesce(var.bucket_name, var.domain)
  force_destroy = var.force_destroy

  tags = {
    Description = "Bucket for ${var.domain}"
    Name        = var.domain
    Project     = var.project_name
  }

  versioning {
    enabled    = var.versioning_enabled
    mfa_delete = var.versioning_mfa_delete
  }
}
