terraform {
  backend "s3" {
    bucket  = "proplogix-terraform"
    encrypt = true
    key     = "terraform-modules-aws.tfstate"
    profile = "proplogix"
    region  = "us-east-1"
  }

  required_providers {
    aws      = "~> 2.70.0"
    local    = "~> 1.0"
    random   = "~> 3.0"
    template = "~> 2.0"
    tls      = "~> 2.0"
  }
}

provider "aws" {
  region  = local.region
  profile = "proplogix"
}

locals {
  environment  = var.environment
  project_name = var.project_name
  region       = "us-east-2"
}

resource "random_string" "password" {
  length      = 24
  lower       = true
  min_upper   = 1
  min_lower   = 1
  min_numeric = 1
  min_special = 0
  number      = true
  upper       = true
  special     = false
}

module "domain" {
  source = "../aws/domain"
  domain = "plx-dev.net"
}

module "env" {
  source = "../common/env"
  domain = "plx-dev.net"
}

module "keypair" {
  source       = "../aws/keypair"
  bucket_name  = module.bucket.name
  domain       = module.env.domain_name
  project_name = local.project_name
}

module "autoscale" {
  source       = "../aws/autoscale"
  ami          = "ami-058043b9f3c10c078"
  domain       = module.env.domain_name
  key_name     = module.keypair.key_name
  name         = local.project_name
  project_name = local.project_name
  region       = local.region
  subnets      = module.vpc.private[*].id
  vpc_id       = module.vpc.vpc_id
}

module "bucket" {
  source         = "../aws/bucket"
  domain         = module.env.domain_name
  bucket_name    = "${local.project_name}.${module.env.domain_name}"
  principal_arns = [module.user.arn]
  project_name   = local.project_name
}

module "certificate" {
  source       = "../aws/certificate"
  domain       = module.env.domain_name
  domain_san   = ["*.${module.env.domain_name}"]
  project_name = local.project_name
  zone_id      = module.domain.zone_id
}

module "loadbalancer" {
  source           = "../aws/loadbalancer"
  certificate_arn  = module.certificate.certificate_arn
  domain           = module.env.domain_name
  project_name     = local.project_name
  subnets          = module.vpc.private[*].id
  target_group_arn = module.autoscale.target_group_arn
  vpc_id           = module.vpc.vpc_id

  rules = [
    {
      host_header      = null
      path_pattern     = "/v1"
      target_group_arn = module.autoscale.target_group_arn
      type             = "forward"
    },
    {
      host_header      = module.env.domain_name
      path_pattern     = null
      target_group_arn = module.autoscale.target_group_arn
      type             = "forward"
    },
  ]
}

module "rds" {
  depends_on             = [module.vpc]
  source                 = "../aws/rds"
  database_identifier    = local.project_name
  database_name          = local.project_name
  domain                 = module.env.domain_name
  bucket_arn             = module.bucket.arn
  db_subnet_group_name   = module.vpc.db_subnet_group_name
  engine                 = "mysql"
  engine_version         = "5.7.26"
  major_engine_version   = "5.7"
  password               = random_string.password.result
  project_name           = local.project_name
  vpc_security_group_ids = []
  vpc_id                 = module.vpc.vpc_id
}

module "secret" {
  source       = "../aws/secret"
  domain       = module.env.domain_name
  project_name = local.project_name
  secret_name  = local.project_name

  secrets = {
    admin = random_string.password.result
  }
}

module "user" {
  source       = "../aws/user"
  domain       = module.env.domain_name
  group_name   = local.project_name
  project_name = local.project_name
  user_name    = local.environment
}

module "vpc" {
  source                         = "../aws/vpc"
  availability_zones             = ["us-east-2a", "us-east-2b"]
  environment                    = local.environment
  project_name                   = local.project_name
  vpc_cidr_block                 = "172.100.0.0/16"
  vpc_cidr_private_block_subnets = ["172.100.16.0/20", "172.100.32.0/20"]
  vpc_cidr_public_block_subnets  = ["172.100.128.0/20", "172.100.144.0/20"]
}



/******************************************************************************
* Third-Party Modules
******************************************************************************/

#module "eventstore" {
#  source               = "git::https://bitbucket.org/plsos2/terraform-aws-eventstore"
#  cluster_azs          = null
#  cluster_name         = local.project_name
#  cluster_subnets      = module.vpc.private[*].id
#  cluster_version      = "5.0.8"
#  cluster_vpc_id       = module.vpc.vpc_id
#  environment          = local.environment
#  key_pair_name        = local.project_name
#  key_pair_publickey   = module.keypair.public_key_openssh
#  instance_type        = module.env.is_production ? "t2.medium" : "t2.micro"
#  instance_volume_size = module.env.is_production ? 8 : 16
#  region               = local.region
#}
