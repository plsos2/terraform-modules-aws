variable "environment" {
  default = "test"
  type    = string
}

variable "project_name" {
  default = "test"
  type    = string
}
