## [7.2.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.2.0...v7.2.1) (2021-05-19)


### Bug Fixes

* aws provider ([e395040](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e395040))
* required_providers syntax ([12cc9da](https://bitbucket.org/plsos2/terraform-modules-aws/commits/12cc9da))

# [7.2.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.1.4...v7.2.0) (2021-05-19)


### Features

* upgrade to 0.13 ([5d23410](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5d23410))

## [7.1.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.1.3...v7.1.4) (2021-05-14)


### Bug Fixes

* vpc tf version ([89c338d](https://bitbucket.org/plsos2/terraform-modules-aws/commits/89c338d))

## [7.1.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.1.2...v7.1.3) (2021-05-14)


### Bug Fixes

* vpc outbound traffic ([5af2312](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5af2312))

## [7.1.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.1.1...v7.1.2) (2021-05-13)


### Bug Fixes

* revert changes to vpc ([2c5eabc](https://bitbucket.org/plsos2/terraform-modules-aws/commits/2c5eabc))

## [7.1.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.1.0...v7.1.1) (2021-05-13)


### Bug Fixes

* TF version ([ad99845](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ad99845))

# [7.1.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.0.4...v7.1.0) (2021-05-13)


### Features

* vpc-find finds arbitrary names ([d72ab5c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/d72ab5c))

## [7.0.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.0.3...v7.0.4) (2020-10-12)


### Bug Fixes

* certificate module ([d2e76c9](https://bitbucket.org/plsos2/terraform-modules-aws/commits/d2e76c9))

## [7.0.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.0.2...v7.0.3) (2020-10-12)


### Bug Fixes

* remove provider from ses ([b0d8aa2](https://bitbucket.org/plsos2/terraform-modules-aws/commits/b0d8aa2))

## [7.0.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.0.1...v7.0.2) (2020-10-12)


### Bug Fixes

* move test into separate folder ([6ac5780](https://bitbucket.org/plsos2/terraform-modules-aws/commits/6ac5780))

## [7.0.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v7.0.0...v7.0.1) (2020-09-04)


### Bug Fixes

* vpc-finder output ([04cfb2b](https://bitbucket.org/plsos2/terraform-modules-aws/commits/04cfb2b))

# [7.0.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v6.4.0...v7.0.0) (2020-09-04)


### Bug Fixes

* 🐛 forcing bump ([35d29b2](https://bitbucket.org/plsos2/terraform-modules-aws/commits/35d29b2))


### BREAKING CHANGES

* 🧨 TF 0.13 and AWS 3.x

# [6.4.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v6.3.0...v6.4.0) (2020-09-04)


### Bug Fixes

* add terraform init ([5d7df83](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5d7df83))
* final snapshot default for rds ([1d98ef1](https://bitbucket.org/plsos2/terraform-modules-aws/commits/1d98ef1))
* keypair and eventstore ([cb51231](https://bitbucket.org/plsos2/terraform-modules-aws/commits/cb51231))
* output openssh pub key ([b6b7833](https://bitbucket.org/plsos2/terraform-modules-aws/commits/b6b7833))
* rds vpc dependency ([246cc8e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/246cc8e))


### Features

* aws 3.x provider support ([8d22aaa](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8d22aaa))

# [6.3.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v6.2.0...v6.3.0) (2020-08-03)


### Features

* add ec2-instance ([5ea2ea4](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5ea2ea4))

# [6.2.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v6.1.0...v6.2.0) (2020-07-07)


### Features

* This adds the option to have the load balancer redirect based on the path pattern in a url. ([32c177e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/32c177e))

# [6.1.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v6.0.0...v6.1.0) (2020-03-17)


### Features

* adding lambda, sns, and sqs modules. ([13ad735](https://bitbucket.org/plsos2/terraform-modules-aws/commits/13ad735))
* adding lambda, sns, and sqs modules. ([20a8bb5](https://bitbucket.org/plsos2/terraform-modules-aws/commits/20a8bb5))
* adding lambda, sns, and sqs modules. ([fe95866](https://bitbucket.org/plsos2/terraform-modules-aws/commits/fe95866))
* adding lambda, sns, and sqs modules. ([176611e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/176611e))

# [6.0.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.6.2...v6.0.0) (2020-02-17)


### Bug Fixes

* arn to id ([187bce8](https://bitbucket.org/plsos2/terraform-modules-aws/commits/187bce8))
* join mssql options with passed in options ([420f4d5](https://bitbucket.org/plsos2/terraform-modules-aws/commits/420f4d5))
* monitoring_role_arn ([c35c165](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c35c165))
* monitoring_role_arn ([5fd2d8d](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5fd2d8d))
* monitoring_role_arn ([c8376a1](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c8376a1))
* monitoring_role_arn checks enable_monitoring ([4cad4e2](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4cad4e2))
* remove mssql default options ([590e2e4](https://bitbucket.org/plsos2/terraform-modules-aws/commits/590e2e4))
* syntax ([f064832](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f064832))
* wrong options for rds changes ([b0ee883](https://bitbucket.org/plsos2/terraform-modules-aws/commits/b0ee883))


### Features

* 🎸 removed option_group_name and parameter_name from rds ([24a1426](https://bitbucket.org/plsos2/terraform-modules-aws/commits/24a1426))
* 🎸 support dynamic rds parameter and options ([9669af8](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9669af8))


### BREAKING CHANGES

* 🧨 removed rds.option_group_name and rds.parameter_name

## [5.6.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.6.1...v5.6.2) (2020-02-12)


### Bug Fixes

* syntax error ([6acec71](https://bitbucket.org/plsos2/terraform-modules-aws/commits/6acec71))

## [5.6.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.6.0...v5.6.1) (2020-02-12)


### Bug Fixes

* vpc default route ([aa95f56](https://bitbucket.org/plsos2/terraform-modules-aws/commits/aa95f56))

# [5.6.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.5.3...v5.6.0) (2020-02-11)


### Bug Fixes

* **rds:** use only one security group ([99e8544](https://bitbucket.org/plsos2/terraform-modules-aws/commits/99e8544))
* 🐛 ecs services ignore_changes ([eef2f5c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/eef2f5c))
* typo ([b14d0fb](https://bitbucket.org/plsos2/terraform-modules-aws/commits/b14d0fb))
* using default route table as private ([80d93e6](https://bitbucket.org/plsos2/terraform-modules-aws/commits/80d93e6))


### Features

* vpc outputs ([28189eb](https://bitbucket.org/plsos2/terraform-modules-aws/commits/28189eb))

## [5.5.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.5.2...v5.5.3) (2020-02-03)


### Bug Fixes

* rds subnets ([9d2853e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9d2853e))

## [5.5.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.5.1...v5.5.2) (2020-02-01)


### Bug Fixes

* add subnet_ids for default nacl ([c86ee21](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c86ee21))

## [5.5.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.5.0...v5.5.1) (2020-02-01)


### Bug Fixes

* remove some depends_on statements ([822db38](https://bitbucket.org/plsos2/terraform-modules-aws/commits/822db38))
* vpc db subnet group ([4c11522](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4c11522))

# [5.5.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.4.3...v5.5.0) (2020-02-01)


### Features

* removing need for vpc_id ([e230f97](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e230f97))

## [5.4.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.4.2...v5.4.3) (2020-02-01)


### Bug Fixes

* removing need for vpc_id to be passed ([007455e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/007455e))

## [5.4.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.4.1...v5.4.2) (2020-02-01)


### Bug Fixes

* availability_zones ([16ca1ce](https://bitbucket.org/plsos2/terraform-modules-aws/commits/16ca1ce))

## [5.4.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.4.0...v5.4.1) (2020-02-01)


### Bug Fixes

* output availability_zones ([9d6b99e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9d6b99e))

# [5.4.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.3.1...v5.4.0) (2020-02-01)


### Bug Fixes

* remove variables no longer needed ([f679886](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f679886))


### Features

* refactored vpc-network ([12afa26](https://bitbucket.org/plsos2/terraform-modules-aws/commits/12afa26))

## [5.3.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.3.0...v5.3.1) (2020-02-01)


### Bug Fixes

* remove optional availability zone ([650b904](https://bitbucket.org/plsos2/terraform-modules-aws/commits/650b904))

# [5.3.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.2.6...v5.3.0) (2020-02-01)


### Features

* re-tooling vpc ([8ed13e8](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8ed13e8))

## [5.2.6](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.2.5...v5.2.6) (2020-02-01)


### Bug Fixes

* variable typo ([f75f57e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f75f57e))

## [5.2.5](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.2.4...v5.2.5) (2020-02-01)


### Bug Fixes

* vpc default cidr block ([c244cc5](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c244cc5))

## [5.2.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.2.3...v5.2.4) (2020-02-01)


### Bug Fixes

* vpc re-creation ([0e73945](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0e73945))

## [5.2.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.2.2...v5.2.3) (2020-02-01)


### Bug Fixes

* gateway references ([2971435](https://bitbucket.org/plsos2/terraform-modules-aws/commits/2971435))

## [5.2.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.2.1...v5.2.2) (2020-02-01)


### Bug Fixes

* add gateway/nat ID ([052ada1](https://bitbucket.org/plsos2/terraform-modules-aws/commits/052ada1))

## [5.2.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.2.0...v5.2.1) (2020-02-01)


### Bug Fixes

* 🐛 vpc-network make environment required ([d22be93](https://bitbucket.org/plsos2/terraform-modules-aws/commits/d22be93))

# [5.2.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.1.5...v5.2.0) (2020-02-01)


### Features

* 🎸 vpc-network add enviroment tag ([30b7ff7](https://bitbucket.org/plsos2/terraform-modules-aws/commits/30b7ff7))

## [5.1.5](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.1.4...v5.1.5) (2020-02-01)


### Bug Fixes

* vpc default route wire to IG ([a9c362e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a9c362e))

## [5.1.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.1.3...v5.1.4) (2020-02-01)


### Bug Fixes

* add availability zone variable ([fbf6a4c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/fbf6a4c))

## [5.1.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.1.2...v5.1.3) (2020-02-01)


### Bug Fixes

* vpc output ([99255ec](https://bitbucket.org/plsos2/terraform-modules-aws/commits/99255ec))

## [5.1.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.1.1...v5.1.2) (2020-02-01)


### Bug Fixes

* vpc id from resource ([001c20d](https://bitbucket.org/plsos2/terraform-modules-aws/commits/001c20d))

## [5.1.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.1.0...v5.1.1) (2020-02-01)


### Bug Fixes

* remove old variables ([e219641](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e219641))

# [5.1.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.0.1...v5.1.0) (2020-01-30)


### Features

* eip and nat/internet gateways managed in vpc ([8d10de9](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8d10de9))

## [5.0.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v5.0.0...v5.0.1) (2020-01-30)


### Bug Fixes

* updated output variables ([fb84247](https://bitbucket.org/plsos2/terraform-modules-aws/commits/fb84247))

# [5.0.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v4.0.0...v5.0.0) (2020-01-29)


### Features

* 🎸 added create_vpc input var ([9f4fff9](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9f4fff9))
* 🎸 vpc and vpc network re-worked ([52c5524](https://bitbucket.org/plsos2/terraform-modules-aws/commits/52c5524))


### BREAKING CHANGES

* 🧨 see changes to vpc and vpc-network

# [4.0.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.10...v4.0.0) (2020-01-29)


### Bug Fixes

* 🐛 output ecs role and task role rather than attributes ([5a74724](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5a74724))


### Features

* **vpc:** accepts project_name when filtering ([2c7b70f](https://bitbucket.org/plsos2/terraform-modules-aws/commits/2c7b70f))


### BREAKING CHANGES

* 🧨 ecs-service output variables have changed

## [3.1.10](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.9...v3.1.10) (2020-01-25)


### Bug Fixes

* **ecs-service:** remove unused output from ([17a99db](https://bitbucket.org/plsos2/terraform-modules-aws/commits/17a99db))

## [3.1.9](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.8...v3.1.9) (2020-01-25)


### Bug Fixes

* adding cidr_blocks to containers ([81d7ad0](https://bitbucket.org/plsos2/terraform-modules-aws/commits/81d7ad0))

## [3.1.8](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.7...v3.1.8) (2020-01-25)


### Bug Fixes

* ecs service output ([e72dd7a](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e72dd7a))

## [3.1.7](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.6...v3.1.7) (2020-01-25)


### Bug Fixes

* long names ([5a32d83](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5a32d83))

## [3.1.6](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.5...v3.1.6) (2020-01-25)


### Bug Fixes

* missing role index use ([20ba420](https://bitbucket.org/plsos2/terraform-modules-aws/commits/20ba420))

## [3.1.5](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.4...v3.1.5) (2020-01-25)


### Bug Fixes

* ecs remove enabled for roles ([73538a5](https://bitbucket.org/plsos2/terraform-modules-aws/commits/73538a5))

## [3.1.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.3...v3.1.4) (2020-01-25)


### Bug Fixes

* role fix in ecs ([1adae8b](https://bitbucket.org/plsos2/terraform-modules-aws/commits/1adae8b))

## [3.1.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.2...v3.1.3) (2020-01-25)


### Bug Fixes

* support for passing in roles ([679ae76](https://bitbucket.org/plsos2/terraform-modules-aws/commits/679ae76))

## [3.1.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.1...v3.1.2) (2020-01-25)


### Bug Fixes

* **ecs-service:** conditional rules when enabled ([c464977](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c464977))

## [3.1.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.1.0...v3.1.1) (2020-01-25)


### Bug Fixes

* reverse rule enumeration ([ffa7853](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ffa7853))

# [3.1.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v3.0.0...v3.1.0) (2020-01-25)


### Features

* ecs modules now output rules ([00b918f](https://bitbucket.org/plsos2/terraform-modules-aws/commits/00b918f))

# [3.0.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.15.5...v3.0.0) (2020-01-24)


### Bug Fixes

* 🐛 removes vpc modules when variable is passed in ([32d3cef](https://bitbucket.org/plsos2/terraform-modules-aws/commits/32d3cef))


### BREAKING CHANGES

* 🧨 removed vpc module as it was causing versioning issues

## [2.15.5](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.15.4...v2.15.5) (2020-01-24)


### Bug Fixes

* fargate remove vpc module ([59f73d0](https://bitbucket.org/plsos2/terraform-modules-aws/commits/59f73d0))

## [2.15.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.15.3...v2.15.4) (2020-01-24)


### Bug Fixes

* making cidr_blocks optional ([6f54ee4](https://bitbucket.org/plsos2/terraform-modules-aws/commits/6f54ee4))

## [2.15.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.15.2...v2.15.3) (2020-01-24)


### Bug Fixes

* ecs-service output ([00f60c7](https://bitbucket.org/plsos2/terraform-modules-aws/commits/00f60c7))

## [2.15.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.15.1...v2.15.2) (2020-01-23)


### Bug Fixes

* update vpc-network to re-use defaults ([431badc](https://bitbucket.org/plsos2/terraform-modules-aws/commits/431badc))
* vpc now separates public/private subnets when tagged ([8ec8ee7](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8ec8ee7))

## [2.15.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.15.0...v2.15.1) (2020-01-22)


### Bug Fixes

* **fargate:** add desired count to ignore list ([cc80706](https://bitbucket.org/plsos2/terraform-modules-aws/commits/cc80706))

# [2.15.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.14.1...v2.15.0) (2020-01-22)


### Bug Fixes

* route table associations ([ee432cf](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ee432cf))


### Features

* adding cidr blocks to containers ([101bdb9](https://bitbucket.org/plsos2/terraform-modules-aws/commits/101bdb9))

## [2.14.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.14.0...v2.14.1) (2020-01-21)


### Bug Fixes

* ecs-fargate expose cluster_id ([78957ec](https://bitbucket.org/plsos2/terraform-modules-aws/commits/78957ec))

# [2.14.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.13.0...v2.14.0) (2020-01-21)


### Bug Fixes

* egress for standard LB SG ([a01e2ff](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a01e2ff))
* lookup defaults on dynamic sg ingress ([d8a7d12](https://bitbucket.org/plsos2/terraform-modules-aws/commits/d8a7d12))
* make lb SGs optional ([4137ca6](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4137ca6))


### Features

* exposing vpc_id ([8320dd1](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8320dd1))

# [2.13.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.12.1...v2.13.0) (2020-01-21)


### Features

* adding default sg for load balancer ([5f0616f](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5f0616f))

## [2.12.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.12.0...v2.12.1) (2020-01-21)


### Bug Fixes

* volumes ([0ec8fad](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0ec8fad))

# [2.12.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.11.0...v2.12.0) (2020-01-21)


### Features

* ecs-services now supports volumes ([33e783a](https://bitbucket.org/plsos2/terraform-modules-aws/commits/33e783a))

# [2.11.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.10.0...v2.11.0) (2020-01-21)


### Bug Fixes

* vpc with ecs ([523f72d](https://bitbucket.org/plsos2/terraform-modules-aws/commits/523f72d))


### Features

* vpc fixes ([c726598](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c726598))

# [2.10.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.9.1...v2.10.0) (2020-01-21)


### Features

* adding separate ecs cluster and services ([01f0c46](https://bitbucket.org/plsos2/terraform-modules-aws/commits/01f0c46))

## [2.9.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.9.0...v2.9.1) (2020-01-21)


### Bug Fixes

* route associations ([0464d66](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0464d66))

# [2.9.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.8.2...v2.9.0) (2020-01-21)


### Features

* vpc network now has nat gateway support ([c85aa25](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c85aa25))

## [2.8.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.8.1...v2.8.2) (2020-01-15)


### Bug Fixes

* version bump. Forgot conventional commit message on last commit. ([28955a1](https://bitbucket.org/plsos2/terraform-modules-aws/commits/28955a1))

## [2.8.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.8.0...v2.8.1) (2020-01-08)


### Bug Fixes

* remove lb output target_group_arns ([fadd860](https://bitbucket.org/plsos2/terraform-modules-aws/commits/fadd860))

# [2.8.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.7.0...v2.8.0) (2020-01-08)


### Features

* adding load balancer target groups ([e7ba65a](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e7ba65a))

# [2.7.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.6.0...v2.7.0) (2020-01-08)


### Features

* adds module_depends_on workaround to lb target group ([7daf7d6](https://bitbucket.org/plsos2/terraform-modules-aws/commits/7daf7d6))

# [2.6.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.5.4...v2.6.0) (2020-01-08)


### Features

* adds module_depends_on workaround to load balancer variables ([fa73957](https://bitbucket.org/plsos2/terraform-modules-aws/commits/fa73957))
* adds module_depends_on workaround to load balancer variables ([2a723bb](https://bitbucket.org/plsos2/terraform-modules-aws/commits/2a723bb))

## [2.5.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.5.3...v2.5.4) (2020-01-07)


### Bug Fixes

* ses interpolation change ([e875a04](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e875a04))

## [2.5.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.5.2...v2.5.3) (2020-01-07)


### Bug Fixes

* ecs-fargate to remove warning about quotes ([4bb5427](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4bb5427))
* keypair remove quotes ([4297ad6](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4297ad6))

## [2.5.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.5.1...v2.5.2) (2020-01-07)


### Bug Fixes

* ses output using splat ([cdb6670](https://bitbucket.org/plsos2/terraform-modules-aws/commits/cdb6670))

## [2.5.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.5.0...v2.5.1) (2019-10-07)


### Bug Fixes

* **eks:** update policy document ([249868e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/249868e))

# [2.5.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.4.1...v2.5.0) (2019-10-04)


### Features

* **rds:** added retention_time parameter ([0cd5c8b](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0cd5c8b))

## [2.4.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.4.0...v2.4.1) (2019-10-04)


### Bug Fixes

* **rds:** fixes name issue ([f5ce230](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f5ce230))

# [2.4.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.3.0...v2.4.0) (2019-10-04)


### Features

* **rds:** added sns topic for event subscriptions ([f870ae3](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f870ae3))

# [2.3.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.2.0...v2.3.0) (2019-09-30)


### Features

* adding health_check_path ([ef54065](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ef54065))

# [2.2.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.1.0...v2.2.0) (2019-09-29)


### Features

* adding task service policies ([4fb7a96](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4fb7a96))

# [2.1.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.0.4...v2.1.0) (2019-09-29)


### Features

* adding ecs task policies ([63c15bc](https://bitbucket.org/plsos2/terraform-modules-aws/commits/63c15bc))

## [2.0.4](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.0.3...v2.0.4) (2019-09-28)


### Bug Fixes

* updated keypair.kms_key_id to return kms_id rather than arn ([50dfa80](https://bitbucket.org/plsos2/terraform-modules-aws/commits/50dfa80))

## [2.0.3](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.0.2...v2.0.3) (2019-09-27)


### Bug Fixes

* remove password_length from ec2-windows ([98909bd](https://bitbucket.org/plsos2/terraform-modules-aws/commits/98909bd))

## [2.0.2](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.0.1...v2.0.2) (2019-09-27)


### Bug Fixes

* ec2-windows custom password ([7458afe](https://bitbucket.org/plsos2/terraform-modules-aws/commits/7458afe))

## [2.0.1](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v2.0.0...v2.0.1) (2019-09-27)


### Bug Fixes

* remove user_data password from ec2-windows ([a4f0755](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a4f0755))

# [2.0.0](https://bitbucket.org/plsos2/terraform-modules-aws/compare/v1.0.0...v2.0.0) (2019-09-27)


### Bug Fixes

* removed ec2-windows user_data defaults ([f43c28a](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f43c28a))


### BREAKING CHANGES

* must provide user_data now in each project

# 1.0.0 (2019-09-27)


### Bug Fixes

*  create before destroy on ec2 sg ([6022582](https://bitbucket.org/plsos2/terraform-modules-aws/commits/6022582))
* add S3 policies ([59c2b27](https://bitbucket.org/plsos2/terraform-modules-aws/commits/59c2b27))
* adding computer_name output and tags ([7268c3a](https://bitbucket.org/plsos2/terraform-modules-aws/commits/7268c3a))
* change log name ([f1c43d8](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f1c43d8))
* cluster name logs ([a468eb5](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a468eb5))
* db identifier defaults to db name ([08aae11](https://bitbucket.org/plsos2/terraform-modules-aws/commits/08aae11))
* db security groups ([d220579](https://bitbucket.org/plsos2/terraform-modules-aws/commits/d220579))
* dependency on private key ([c49f90c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c49f90c))
* disable MFA requirement ([e374d49](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e374d49))
* ecs, fargate, and vpc ([93e686f](https://bitbucket.org/plsos2/terraform-modules-aws/commits/93e686f))
* enable_deletion_protection ([333a0b9](https://bitbucket.org/plsos2/terraform-modules-aws/commits/333a0b9))
* encrypted storage ([34cc5b9](https://bitbucket.org/plsos2/terraform-modules-aws/commits/34cc5b9))
* fixing names ([66fe8ee](https://bitbucket.org/plsos2/terraform-modules-aws/commits/66fe8ee))
* increase default disk size ([7e06631](https://bitbucket.org/plsos2/terraform-modules-aws/commits/7e06631))
* license model for mssql rds ([0c44a37](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0c44a37))
* load balancer ([a9678c5](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a9678c5))
* lock down S3 policies for rds backups ([8c7eefa](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8c7eefa))
* longer windows password ([cce8d96](https://bitbucket.org/plsos2/terraform-modules-aws/commits/cce8d96))
* making ec2 logs unit ([f63da20](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f63da20))
* making resource names unique to app ([669a6fc](https://bitbucket.org/plsos2/terraform-modules-aws/commits/669a6fc))
* map rds variables to params ([dc761d3](https://bitbucket.org/plsos2/terraform-modules-aws/commits/dc761d3))
* more domain env shuffling ([a417723](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a417723))
* naming ([f620cd6](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f620cd6))
* no longer adding route53 resource ([869649c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/869649c))
* normalize deletion ([0f2ddf5](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0f2ddf5))
* normalize names ([554b734](https://bitbucket.org/plsos2/terraform-modules-aws/commits/554b734))
* rdp port ([642056e](https://bitbucket.org/plsos2/terraform-modules-aws/commits/642056e))
* rds params ([95ab689](https://bitbucket.org/plsos2/terraform-modules-aws/commits/95ab689))
* rds s3 policy arn ([5dd8dde](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5dd8dde))
* remove lifecycle ignoring desired_count ([f143bd6](https://bitbucket.org/plsos2/terraform-modules-aws/commits/f143bd6))
* remove snapshot ([ace4100](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ace4100))
* remove ssh port ([ed0fb1b](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ed0fb1b))
* remove vpc ([229a54d](https://bitbucket.org/plsos2/terraform-modules-aws/commits/229a54d))
* removed sid ([c750a8c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c750a8c))
* removing defaults for vpc network ([ae171e2](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ae171e2))
* resource count enhanced_monitoring ([0cbf1f4](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0cbf1f4))
* revert ec2 instance security groups ([dcc3d14](https://bitbucket.org/plsos2/terraform-modules-aws/commits/dcc3d14))
* set senstive_content for pem ([34241ad](https://bitbucket.org/plsos2/terraform-modules-aws/commits/34241ad))
* target groups ([5343202](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5343202))
* update correct role for ECS permissions ([9e81c17](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9e81c17))
* update cpu_threads_per_core ([8522649](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8522649))
* update ec2 passwords and provisioners ([8a77a90](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8a77a90))
* update instance security groups ([66ee486](https://bitbucket.org/plsos2/terraform-modules-aws/commits/66ee486))
* update monitoring indexer ([82ac7b3](https://bitbucket.org/plsos2/terraform-modules-aws/commits/82ac7b3))
* update rds port to security groups only ([8a43ebb](https://bitbucket.org/plsos2/terraform-modules-aws/commits/8a43ebb))
* update user data ([26663a4](https://bitbucket.org/plsos2/terraform-modules-aws/commits/26663a4))
* update user policies ([9007d15](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9007d15))


### Features

* add rds options ([a649cc2](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a649cc2))
* adding db option groups ([a14cac4](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a14cac4))
* adding environment tag ([da5dfca](https://bitbucket.org/plsos2/terraform-modules-aws/commits/da5dfca))
* adding iam ses user ([cb276f7](https://bitbucket.org/plsos2/terraform-modules-aws/commits/cb276f7))
* adding ingress to ec2 windows instances ([32b3878](https://bitbucket.org/plsos2/terraform-modules-aws/commits/32b3878))
* adding lb rules ([deda28b](https://bitbucket.org/plsos2/terraform-modules-aws/commits/deda28b))
* adding more ses output vars ([35d0991](https://bitbucket.org/plsos2/terraform-modules-aws/commits/35d0991))
* adding parameters ([2511162](https://bitbucket.org/plsos2/terraform-modules-aws/commits/2511162))
* adding password output ([245d6b0](https://bitbucket.org/plsos2/terraform-modules-aws/commits/245d6b0))
* adding project_name ([4cee905](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4cee905))
* adding rds vpc for fargate ([3c68603](https://bitbucket.org/plsos2/terraform-modules-aws/commits/3c68603))
* adding security group for db ([ec7b01f](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ec7b01f))
* adding ses ([123bd2c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/123bd2c))
* adding user module ([da93865](https://bitbucket.org/plsos2/terraform-modules-aws/commits/da93865))
* condensing security groups ([ffa51cd](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ffa51cd))
* domain shuffle ([4e5f39c](https://bitbucket.org/plsos2/terraform-modules-aws/commits/4e5f39c))
* dynamic ebs block devices ([e6c5156](https://bitbucket.org/plsos2/terraform-modules-aws/commits/e6c5156))
* enable auditing ([1779ec2](https://bitbucket.org/plsos2/terraform-modules-aws/commits/1779ec2))
* first release version ([a51c058](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a51c058))
* force delete protection in production ([5cbc6e3](https://bitbucket.org/plsos2/terraform-modules-aws/commits/5cbc6e3))
* format user data ([c407edf](https://bitbucket.org/plsos2/terraform-modules-aws/commits/c407edf))
* huge refactor ([0d1ba4f](https://bitbucket.org/plsos2/terraform-modules-aws/commits/0d1ba4f))
* ignoring changes to task definitions ([9b9f079](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9b9f079))
* major refactor ([049e29b](https://bitbucket.org/plsos2/terraform-modules-aws/commits/049e29b))
* rds added iam_database_authentication ([b050948](https://bitbucket.org/plsos2/terraform-modules-aws/commits/b050948))
* rds added iam_database_authentication ([ca15169](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ca15169))
* update autoscale ([ab1d683](https://bitbucket.org/plsos2/terraform-modules-aws/commits/ab1d683))
* update bucket output ([d177951](https://bitbucket.org/plsos2/terraform-modules-aws/commits/d177951))
* update domains ([9abb2b9](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9abb2b9))
* updating env module variables ([1afc806](https://bitbucket.org/plsos2/terraform-modules-aws/commits/1afc806))
* using name prefix for all security groups ([9e51a8b](https://bitbucket.org/plsos2/terraform-modules-aws/commits/9e51a8b))
* vpc network ([a441b8f](https://bitbucket.org/plsos2/terraform-modules-aws/commits/a441b8f))


### BREAKING CHANGES

* remove vpc
