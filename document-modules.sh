#!/bin/bash

function document_module() {
    MODULE=$1
    MD_MODULE=${MODULE}/MODULE.md
    MD_README=${MODULE}/README.md

    MODULE_DIR=$(dirname $MODULE)
    MODULE_NAME=$(basename $MODULE)

    if [ ! -f $MD_README ]; then
        echo "Creating default README.md for ${MODULE_NAME}"

        cat <<EOF >$MD_README
# $MODULE_NAME
${MODULE_DIR^^} Module $MODULE_NAME

# Links
Additional links and information:
- [Terraform](https://www.terraform.io/docs/index.html)
- [Documentation](MODULE.md)
EOF
    fi

    echo "Updating ${MODULE_NAME} MODULE.md"
    terraform-docs markdown $MODULE >$MD_MODULE
}

for MODULE in aws/*; do
    document_module $MODULE
done

for MODULE in common/*; do
    document_module $MODULE
done

GIT_COMMITTER_NAME="documenter" \
    GIT_COMMITTER_EMAIL="dev@proplogix.com" \
    git diff-index --quiet HEAD -- ||
    git add -A . &&
    git commit -m "chore(documentation)" ||
    true
