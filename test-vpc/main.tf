terraform {
  required_providers {
    aws      = "~> 2.70.0"
    local    = "~> 1.0"
    random   = "~> 3.0"
    template = "~> 2.0"
    tls      = "~> 2.0"
  }
}

locals {
  environment  = "test"
  project_name = "test-vpc"
  region       = "us-east-2"
}

provider "aws" {
  region  = local.region
  profile = "proplogix"
}

module "vpc" {
  source                         = "../aws/vpc"
  availability_zones             = ["us-east-2a", "us-east-2b"]
  environment                    = local.environment
  project_name                   = local.project_name
  vpc_cidr_block                 = "172.100.0.0/16"
  vpc_cidr_private_block_subnets = ["172.100.16.0/20", "172.100.32.0/20"]
  vpc_cidr_public_block_subnets  = ["172.100.128.0/20", "172.100.144.0/20"]
}

#module "vpc_infrastructure" {
#  source  = "terraform-aws-modules/vpc/aws"
#  version = "~> 2.47"

#  name                 = "${local.project_name}-${local.environment}-vpc"
#  cidr                 = "172.33.0.0/16"
#  azs                  = ["us-east-2a", "us-east-2b", "us-east-2c", "us-east-2d"]
#  private_subnets      = ["172.33.1.0/24", "172.33.2.0/24", "172.33.3.0/24"]
#  public_subnets       = ["172.33.4.0/24", "172.33.5.0/24", "172.33.6.0/24"]
#  enable_nat_gateway   = true
#  single_nat_gateway   = true
#  enable_dns_hostnames = true
#}
